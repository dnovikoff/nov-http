#include <nov/http/parser.hpp>
#include <nov/http/response_handler.hpp>
#include <nov/http/state_parser.hpp>

#include "test.hpp"

using NovHttp::Buffer;
using NovHttp::CStringView;
using NovHttp::Parser;
using NovHttp::StateParser;
using NovHttp::BodyInfo;
using NovHttp::FailReason;

class TestHandler: public NovHttp::ResponseHandler {
public:
	void status(const CStringView& version, int code, const CStringView& reason) override {
		log << "status(" << version << ", " << code << ", " << reason<< ")\n";
	}
	void header(const CStringView& key, const CStringView& value) override {
		log << "header(" << key << ", " << value << ")\n";
	}
	bool body(const CStringView& chunk) override {
		log << "chunk(" << chunk << ")\n";
		return true;
	}
	bool done() override {
		log << "done()\n";
		return true;
	}
	void fail(FailReason, const std::string&) override {
		log << "fail()\n";
	}
	void headersDone(const BodyInfo& info) override {
		log << "headersDone(body=" << info.expectBody << ", chunked=" << info.chunked << ", length=" << info.length << ")\n";
	}
	void headerValueNextLine(const CStringView& valueChunk) override {
		log << "headerValueNextLine(" << valueChunk << ")\n";
	}

	std::ostringstream log;
};

struct HttpTester {
	explicit HttpTester(bool withBody = false): buffer(mem), stateParser(&handler, withBody) {
		parser.setBuffer(buffer);
	}

	std::string parse() {
		auto state = stateParser.parse(buffer);

		return (state==Parser::Pause?"Paused: ":"")+handler.log.str();
	}

	template<std::size_t Size>
	inline void write(const char(&x)[Size]) {
		memcpy(buffer.buffer().pointer, x, Size-1);
		buffer.add(Size-1);
	}

	inline Parser::State parseStatus() {
		return parser.parseStatus(version, code, reason);
	}

	inline Parser::State parseHeader() {
		return parser.parseHeader(key, value);
	}

	inline Parser::State parseChunkHeader() {
		return parser.parseChunkHeader(chunkSize);
	}

	inline Parser::State parseChunkMiddle(std::size_t size) {
		return parser.parseChunkMiddle(size, chunk);
	}


	char mem[256];
	Buffer buffer;
	Parser parser;

	CStringView version;
	int code = 0;
	std::size_t chunkSize = 0;
	CStringView reason;
	CStringView key;
	CStringView value;
	CStringView chunk;
	TestHandler handler;
	StateParser stateParser;
};


template<std::size_t Size>
static Parser::State checkStatusFailed(const char(&x)[Size]) {
	HttpTester t;
	t.write(x);
	auto stat = t.parseStatus();
//	std::cout << static_cast<int>(stat)  << " != " << static_cast<int>(Parser::Failed) << std::endl;
	return stat;
}

template<std::size_t Size>
static std::string checkStatusParsed(const char(&x)[Size]) {
	HttpTester t;
	t.write(x);
	auto stat = t.parseStatus();
	std::ostringstream oss;
	if (stat == Parser::Header) {
		oss << t.version << " " << t.code << " " << t.reason;
	} else {
		oss << "Parse failed";
	}
	return oss.str();
}

template<std::size_t Size>
static std::string checkHeaderParsed(const char(&x)[Size]) {
	HttpTester t;
	t.write(x);
	auto stat = t.parseHeader();
	std::ostringstream oss;
	if (stat == Parser::Body) {
		oss << "Done";
	} else if (stat == Parser::Header) {
		oss << t.key.toString() << ": " << t.value.toString();
	} else if (stat == Parser::HeaderMore) {
		oss << "MORE: " << t.value.toString();
	} else {
		oss << "Failed";
	}
	return oss.str();
}


template<std::size_t Size>
static std::string checkChunkHeaderParsed(const char(&x)[Size]) {
	HttpTester t;
	t.write(x);
	auto stat = t.parseChunkHeader();
	std::ostringstream oss;
	if (stat == Parser::ChunkMiddle) {
		oss << t.chunkSize;
	} else if (stat == Parser::Pause) {
		oss << "Pause";
	} else {
		oss << "Failed";
	}
	return oss.str();
}

template<std::size_t Size>
static std::string checkChunkMiddleParsed(const char(&x)[Size], std::size_t size) {
	HttpTester t;
	t.write(x);
	auto stat = t.parseChunkMiddle(size);
	std::ostringstream oss;
	oss << "'" << t.chunk.toString() << "'" << ": ";
	if (stat == Parser::Pause) {
		oss << "Pause";
	} else if (stat == Parser::ChunkStart) {
		oss << "Next";
	} else {
		oss << "Failed";
	}
	return oss.str();
}

BOOST_AUTO_TEST_CASE (status) {
	HttpTester t;
	t.write("HTTP/1.1 404 Not Found");
	auto status = t.parseStatus();
	BOOST_REQUIRE(status == Parser::Pause);
	t.write("\r\n");
	status = t.parseStatus();
	BOOST_REQUIRE(status == Parser::Header);
	BOOST_REQUIRE_EQUAL("HTTP/1.1", t.version.toString());
	BOOST_REQUIRE_EQUAL(404, t.code);
	BOOST_REQUIRE_EQUAL("Not Found", t.reason.toString());
}

BOOST_AUTO_TEST_CASE (status_digits) {
	BOOST_CHECK(Parser::Failed == checkStatusFailed("HTTP/1.1 ABC Not Found\r\n"));
	BOOST_CHECK(Parser::Failed == checkStatusFailed("HTTP/1.1 1984 Not Found\r\n"));
	BOOST_CHECK(Parser::Failed == checkStatusFailed("HTTP/1.1 ABC Not Found\r\n"));
	BOOST_CHECK(Parser::Failed == checkStatusFailed("HTTP/1.1 Found\r\n"));

	BOOST_CHECK(Parser::Failed == checkStatusFailed("HTTP/1.1 -98 Not Found\r\n"));
	BOOST_CHECK(Parser::Failed == checkStatusFailed("HTTP/1.1 +98 Not Found\r\n"));

	BOOST_CHECK(Parser::Header == checkStatusFailed("HTTP/1.1 198 Not Found\r\n"));
}

BOOST_AUTO_TEST_CASE (status_endline_tolerance) {
	BOOST_CHECK_EQUAL("HTTP/1.1 198 Message", checkStatusParsed("HTTP/1.1 198 Message\n"));
	BOOST_CHECK_EQUAL("HTTP/1.1 198 Message", checkStatusParsed("HTTP/1.1 198 Message\r\n"));
}

BOOST_AUTO_TEST_CASE (status_space_tolerance) {
	const std::string e = "HTTP/1.1 200 Message";
	BOOST_CHECK_EQUAL(e, checkStatusParsed("HTTP/1.1\t200 Message\r\n"));
	BOOST_CHECK_EQUAL(e, checkStatusParsed("HTTP/1.1 200\tMessage\r\n"));
	BOOST_CHECK_EQUAL(e, checkStatusParsed("HTTP/1.1\t200\tMessage\r\n"));
	BOOST_CHECK_EQUAL(e, checkStatusParsed("HTTP/1.1\t \t200                        Message\r\n"));
	BOOST_CHECK_EQUAL(e, checkStatusParsed("HTTP/1.1\t \t200\t             \t \tMessage\r\n"));
}

BOOST_AUTO_TEST_CASE (header_regular) {
	const std::string e = "Hello: World";
	BOOST_CHECK_EQUAL(e, checkHeaderParsed("Hello: World\r\n"));
	BOOST_CHECK_EQUAL(e, checkHeaderParsed("Hello: World\n"));
	BOOST_CHECK_EQUAL(e, checkHeaderParsed("Hello:World\r\n"));
	BOOST_CHECK_EQUAL(e, checkHeaderParsed("Hello:   World\r\n"));
	BOOST_CHECK_EQUAL(e, checkHeaderParsed("Hello:\tWorld\r\n"));

	BOOST_CHECK_EQUAL("Hello : World", checkHeaderParsed("Hello : World\r\n"));
}

BOOST_AUTO_TEST_CASE (header_more) {
	const std::string e = "MORE: Hello: World";
	BOOST_CHECK_EQUAL(e, checkHeaderParsed(" Hello: World\r\n"));
	BOOST_CHECK_EQUAL(e, checkHeaderParsed("\tHello: World\r\n"));
	BOOST_CHECK_EQUAL(e, checkHeaderParsed("        \t   Hello: World\r\n"));
}

BOOST_AUTO_TEST_CASE (header_done) {
	BOOST_CHECK_EQUAL("Done", checkHeaderParsed("\r\n"));
}

BOOST_AUTO_TEST_CASE (header_fail) {
	BOOST_CHECK_EQUAL("Failed", checkHeaderParsed("1\r\n"));
	BOOST_CHECK_EQUAL("Failed", checkHeaderParsed("a\r\n"));
	BOOST_CHECK_EQUAL("Failed", checkHeaderParsed("\r\t\n"));
	BOOST_CHECK_EQUAL("Failed", checkHeaderParsed("Key value\n"));
}

BOOST_AUTO_TEST_CASE (chunk_header) {
	BOOST_CHECK_EQUAL("1", checkChunkHeaderParsed("1\r\n"));
	BOOST_CHECK_EQUAL("0", checkChunkHeaderParsed("0\r\n"));
	BOOST_CHECK_EQUAL("15", checkChunkHeaderParsed("F\r\n"));
	BOOST_CHECK_EQUAL("15", checkChunkHeaderParsed("F\n"));
	BOOST_CHECK_EQUAL("31", checkChunkHeaderParsed("1f\r\n"));

	BOOST_CHECK_EQUAL("Failed", checkChunkHeaderParsed("\r\n"));
	BOOST_CHECK_EQUAL("Failed", checkChunkHeaderParsed("-\r\n"));
	BOOST_CHECK_EQUAL("Failed", checkChunkHeaderParsed("+\r\n"));
	BOOST_CHECK_EQUAL("Failed", checkChunkHeaderParsed("-1\r\n"));
	BOOST_CHECK_EQUAL("Failed", checkChunkHeaderParsed("+1\r\n"));

	BOOST_CHECK_EQUAL("Pause", checkChunkHeaderParsed(""));
	BOOST_CHECK_EQUAL("Pause", checkChunkHeaderParsed("1"));
	BOOST_CHECK_EQUAL("Pause", checkChunkHeaderParsed("12354"));
	BOOST_CHECK_EQUAL("Pause", checkChunkHeaderParsed("xyz"));
	BOOST_CHECK_EQUAL("Failed", checkChunkHeaderParsed("xyz\r\n"));
	BOOST_CHECK_EQUAL("Failed", checkChunkHeaderParsed(" 1\r\n"));
}


BOOST_AUTO_TEST_CASE (chunk_middle) {
	BOOST_CHECK_EQUAL("'1': Next", checkChunkMiddleParsed("1\r\nOther part", 1));
	BOOST_CHECK_EQUAL("'1': Next", checkChunkMiddleParsed("1\nOther part", 1));

	BOOST_CHECK_EQUAL("'': Next", checkChunkMiddleParsed("\n", 0));
	BOOST_CHECK_EQUAL("'': Next", checkChunkMiddleParsed("\r\n", 0));
	BOOST_CHECK_EQUAL("'': Pause", checkChunkMiddleParsed("\r", 0));
	BOOST_CHECK_EQUAL("'': Pause", checkChunkMiddleParsed("", 0));

	BOOST_CHECK_EQUAL("'1\nOther part': Next", checkChunkMiddleParsed("1\nOther part\n", 12));
	BOOST_CHECK_EQUAL("'1\nOth': Pause", checkChunkMiddleParsed("1\nOth", 5));
	BOOST_CHECK_EQUAL("'1\nOth': Pause", checkChunkMiddleParsed("1\nOth\r", 5));
	BOOST_CHECK_EQUAL("'1\nOth': Next", checkChunkMiddleParsed("1\nOth\r\n", 5));
}

BOOST_AUTO_TEST_CASE (head) {
	HttpTester t;
	t.write("HTTP/1.1 200 OK\nContent-LENGTH: 200\n\n");
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"header(Content-LENGTH, 200)\n"
		"headersDone(body=0, chunked=0, length=200)\n"
		"done()\n"
	;

	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (no_headers) {
	HttpTester t;
	t.write("HTTP/1.1 200 OK\n\n");
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"headersDone(body=0, chunked=0, length=0)\n"
		"done()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (chunked) {
	HttpTester t(true);

	t.write(
		"HTTP/1.1 200 OK\n"
		"Transfer-Encoding: chunked\r\n"
		"\r\n5\nHello\n6\n World\n0\n\n"
	);
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"header(Transfer-Encoding, chunked)\n"
		"headersDone(body=1, chunked=1, length=0)\n"
		"chunk(Hello)\n"
		"chunk( World)\n"
		"done()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (identity) {
	HttpTester t;

	t.write(
		"HTTP/1.1 200 OK\n"
		"Transfer-Encoding: identity\n\n"
	);
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"header(Transfer-Encoding, identity)\n"
		"headersDone(body=0, chunked=0, length=0)\n"
		"done()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (length) {
	HttpTester t(true);

	t.write(
		"HTTP/1.1 200 OK\n"
		"Content-Length: 10\n"
		"\n"
		"1234567890"
	);
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"header(Content-Length, 10)\n"
		"headersDone(body=1, chunked=0, length=10)\n"
		"chunk(1234567890)\n"
		"done()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (length_and_chunked) {
	HttpTester t(true);

	t.write(
		"HTTP/1.1 200 OK\n"
		"Content-Length: 10\n"
		"Transfer-Encoding: chunked\r\n"
		"\r\n5\nHello\n6\n World\n0\n\n"
	);
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"header(Content-Length, 10)\n"
		"header(Transfer-Encoding, chunked)\n"
		"headersDone(body=1, chunked=1, length=10)\n"
		"chunk(Hello)\n"
		"chunk( World)\n"
		"done()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (no_length_and_chunked) {
	HttpTester t(true);

	t.write(
		"HTTP/1.1 200 OK\n"
		"\r\n5\nHello\n6\n World\n0\n\n"
	);
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"fail()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (special_1xx) {
	HttpTester t(true);
	t.write("HTTP/1.1 123 OK\n\n");
	auto expected =
		"status(HTTP/1.1, 123, OK)\n"
		"headersDone(body=0, chunked=0, length=0)\n"
		"done()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (special_204) {
	HttpTester t(true);
	t.write("HTTP/1.1 204 OK\n\n");
	auto expected =
		"status(HTTP/1.1, 204, OK)\n"
		"headersDone(body=0, chunked=0, length=0)\n"
		"done()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (special_304) {
	HttpTester t(true);
	t.write("HTTP/1.1 304 OK\n\n");
	auto expected =
		"status(HTTP/1.1, 304, OK)\n"
		"headersDone(body=0, chunked=0, length=0)\n"
		"done()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (not_special_305) {
	HttpTester t(true);
	t.write("HTTP/1.1 305 OK\n\n");
	auto expected =
		"status(HTTP/1.1, 305, OK)\n"
		"fail()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (extra_header_value) {
	HttpTester t;
	t.write(
		"HTTP/1.1 200 OK\n"
		"Key: 10\n"
		" More\r\n"
		"\n"
	);
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"header(Key, 10)\n"
		"headerValueNextLine(More)\n"
		"headersDone(body=0, chunked=0, length=0)\n"
		"done()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (wrong_content_length) {
	HttpTester t;
	t.write(
		"HTTP/1.1 200 OK\n"
		"Content-Length: -6\n"
		"\n"
	);
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"fail()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (large_content_length) {
	HttpTester t;
	t.write(
		"HTTP/1.1 200 OK\n"
		"Content-Length: 999999999999999999999\n"
		"\n"
	);
	auto expected =
		"status(HTTP/1.1, 200, OK)\n"
		"fail()\n"
	;
	BOOST_CHECK_EQUAL(expected, t.parse());
}

BOOST_AUTO_TEST_CASE (pause_cases) {
	HttpTester t(true);

	t.write(
		"HTTP/1.1 200"
	);
	BOOST_CHECK_EQUAL("Paused: ", t.parse());
	t.write(
		" OK\nContent-Length:"
	);
	BOOST_CHECK_EQUAL(
		"Paused: status(HTTP/1.1, 200, OK)\n"
		, t.parse());
	t.write(
		"10\n\n1"
	);
	BOOST_CHECK_EQUAL(
		"Paused: status(HTTP/1.1, 200, OK)\n"
		"header(Content-Length, 10)\n"
		"headersDone(body=1, chunked=0, length=10)\n"
		"chunk(1)\n"
		, t.parse());
	t.write(
		"234567890junkjunk"
	);
	BOOST_CHECK_EQUAL(
		"status(HTTP/1.1, 200, OK)\n"
		"header(Content-Length, 10)\n"
		"headersDone(body=1, chunked=0, length=10)\n"
		"chunk(1)\n"
		"chunk(234567890)\n"
		"done()\n"
		, t.parse());
}

BOOST_AUTO_TEST_CASE (chunk_bug_1) {
	HttpTester t(true);
	t.write(
		"HTTP/1.1 200 OK\r\n"
		"Transfer-Encoding: chunked\r\n\r\n"
		"99\r\n not enought"
	);
	BOOST_CHECK_EQUAL(
		"Paused: status(HTTP/1.1, 200, OK)\n"
		"header(Transfer-Encoding, chunked)\n"
		"headersDone(body=1, chunked=1, length=0)\n"
		"chunk( not enought)\n"
		, t.parse());
}
