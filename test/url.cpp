#include <nov/http/url.hpp>

#include "test.hpp"

using NovHttp::Url;


std::string checkUrl(const std::string& url) {
	Url u;
	if (!u.parse(url)) {
		return "Invalid";
	}
	std::ostringstream oss;
	oss << "service=" << u.portStr();
	oss << " host=" << u.host();
	oss << " path=" << u.path();
	if (u.isSsl()) {
		oss << " ssl";
	}
	if (u.isIp()) {
		oss << " ip";
	}
	return oss.str();
}

BOOST_AUTO_TEST_CASE (simple) {
	BOOST_CHECK_EQUAL("service=443 host=example.com path=/ ssl", checkUrl("https://example.com/"));
	BOOST_CHECK_EQUAL("service=443 host=example.com path=/ ssl", checkUrl("https://example.com"));
	BOOST_CHECK_EQUAL("service=80 host=example.com path=/", checkUrl("http://example.com/"));
	BOOST_CHECK_EQUAL("service=443 host=example.com path=/", checkUrl("http://example.com:443/"));
	BOOST_CHECK_EQUAL("service=80 host=example.com path=/ ssl", checkUrl("https://example.com:80/"));
	BOOST_CHECK_EQUAL("service=80 host=example.com path=/index.html", checkUrl("http://example.com/index.html"));
}

BOOST_AUTO_TEST_CASE (ip) {
	BOOST_CHECK_EQUAL("service=80 host=192.168.1.1 path=/ ip", checkUrl("http://192.168.1.1/"));
	BOOST_CHECK_EQUAL("service=80 host=292.168.1.1 path=/", checkUrl("http://292.168.1.1/"));
}

