#include "tester.hpp"

#include <nov/http/redirect_response_decorator.hpp>

BOOST_AUTO_TEST_CASE (no_redirect) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/");
	req.timeouts.setAllMs(1000);
	auto data = std::make_unique<NovHttp::RedirectResponseDecorator::Data>();
	data->attempts = 10;
	data->prototype = req.copy();
	data->fetcher = &tester.fetcher;
	data->original = std::make_unique<StateHandler>(*result);
	req.handler = std::make_unique<NovHttp::RedirectResponseDecorator>(std::move(data));

	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCSD", result->states());
}

BOOST_AUTO_TEST_CASE (no_redirect_post) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/");
	req.timeouts.setAllMs(1000);
	req.method = "POST";
	req.body = NovHttp::StringBodyProvider::create("Hello");
	auto data = std::make_unique<NovHttp::RedirectResponseDecorator::Data>();
	data->attempts = 10;
	data->prototype = req.copy();
	data->fetcher = &tester.fetcher;
	data->original = std::make_unique<StateHandler>(*result);
	req.handler = std::make_unique<NovHttp::RedirectResponseDecorator>(std::move(data));

	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCSD", result->states());
}

BOOST_AUTO_TEST_CASE (redirect_ok) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/redirect/2");
	req.timeouts.setAllMs(1000);
	auto data = std::make_unique<NovHttp::RedirectResponseDecorator::Data>();
	data->attempts = 2;
	data->prototype = req.copy();
	data->fetcher = &tester.fetcher;
	data->original = std::make_unique<StateHandler>(*result);
	req.handler = std::make_unique<NovHttp::RedirectResponseDecorator>(std::move(data));
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCSSSD", result->states());
}

BOOST_AUTO_TEST_CASE (redirect_too_much) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/redirect/2");
	req.timeouts.setAllMs(1000);
	auto data = std::make_unique<NovHttp::RedirectResponseDecorator::Data>();
	data->attempts = 1;
	data->prototype = req.copy();
	data->fetcher = &tester.fetcher;
	data->original = std::make_unique<StateHandler>(*result);
	req.handler = std::make_unique<NovHttp::RedirectResponseDecorator>(std::move(data));
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCSSF", result->states());
}

BOOST_AUTO_TEST_CASE (redirect_abs) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/absolute-redirect/2");
	req.timeouts.setAllMs(1000);
	auto data = std::make_unique<NovHttp::RedirectResponseDecorator::Data>();
	data->attempts = 2;
	data->prototype = req.copy();
	data->fetcher = &tester.fetcher;
	data->original = std::make_unique<StateHandler>(*result);
	req.handler = std::make_unique<NovHttp::RedirectResponseDecorator>(std::move(data));
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCSSSD", result->states());
}
