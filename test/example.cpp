#include "tester.hpp"

BOOST_AUTO_TEST_CASE (run_case) {
	Tester tester;
	auto req = RequestHelper::createFromUrl("http://example.com/");
	req.handler.reset(new LogHandler);
	RequestHelper::addClose(req);
	auto f1 = tester.fetcher.fetch(std::move(req));
	tester.io.run();

	BOOST_REQUIRE(true);
}

BOOST_AUTO_TEST_CASE (run_ssl) {
	Tester tester;
	auto req = RequestHelper::createFromUrl("https://example.com/");
	BOOST_REQUIRE(req.url.isSsl());
	req.handler.reset(new LogHandler);
	RequestHelper::addClose(req);
	req.timeouts.handshake = std::chrono::seconds(1);
	auto f1 = tester.fetcher.fetch(std::move(req));
	tester.io.run();

	BOOST_REQUIRE(true);
}

BOOST_AUTO_TEST_CASE (deflate) {
	Tester tester;
	auto req = RequestHelper::createFromUrl("https://ya.ru");
	req.handler.reset(new LogHandler);
	RequestHelper::addDecoder(req);
	req.timeouts.handshake = std::chrono::seconds(1);
	auto f1 = tester.fetcher.fetch(std::move(req));
	tester.io.run();

	BOOST_REQUIRE(true);
}

BOOST_AUTO_TEST_CASE (create) {
	Tester tester;
	auto req = RequestHelper::createFromUrl("https://example.com/index.html");
	req.handler.reset(new LogHandler);
	auto f1 = tester.fetcher.fetch(std::move(req));
	tester.io.run();

	BOOST_REQUIRE(true);
}

BOOST_AUTO_TEST_CASE (case_cached) {
	Tester tester;
	auto req = RequestHelper::createFromUrl("http://example.com/index.html");
	req.connectionClose = true;
	auto result = std::make_unique<TestResult>();
	req.handler = std::make_unique<StateHandler>(*result);
	auto f = tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCSD", result->states());

	req = RequestHelper::createFromUrl("http://example.com");
	req.endopoint = f->request().endopoint;
	result = std::make_unique<TestResult>();
	req.handler = std::make_unique<StateHandler>(*result);
	f = tester.fetcher.fetch(std::move(req));
	tester.io.reset();
	tester.io.run();
	BOOST_CHECK_EQUAL("CSD", result->states());

	req = RequestHelper::createFromUrl("http://example.com");
	req.socket = f->request().socket;
	result = std::make_unique<TestResult>();
	req.handler = std::make_unique<StateHandler>(*result);
	f = tester.fetcher.fetch(std::move(req));
	tester.io.reset();
	tester.io.run();
	BOOST_CHECK_EQUAL("SD", result->states());
}

BOOST_AUTO_TEST_CASE (cached_tls) {
	Tester tester;
	auto req = RequestHelper::createFromUrl("https://example.com/index.html");
	auto result = std::make_unique<TestResult>();
	req.handler = std::make_unique<StateHandler>(*result);
	auto f = tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCHSD", result->states());

	req = RequestHelper::createFromUrl("https://example.com");
	req.socket = f->request().socket;
	result = std::make_unique<TestResult>();
	req.handler = std::make_unique<StateHandler>(*result);
	f = tester.fetcher.fetch(std::move(req));
	tester.io.reset();
	tester.io.run();
	BOOST_CHECK_EQUAL("SD", result->states());
}

BOOST_AUTO_TEST_CASE (inner_cache_usage) {
	auto result = std::make_unique<TestResult>();

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://example.com/1");
	req.handler = std::make_unique<StateHandler>(*result);
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCSD", result->states());

	result = std::make_unique<TestResult>();
	req = RequestHelper::createFromUrl("http://example.com/2");
	req.handler = std::make_unique<StateHandler>(*result);
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("SD", result->states());
}

BOOST_AUTO_TEST_CASE (inner_cached_tls) {
	auto result = std::make_unique<TestResult>();

	Tester tester;
	auto req = RequestHelper::createFromUrl("https://example.com/1");
	req.handler = std::make_unique<StateHandler>(*result);
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCHSD", result->states());

	result = std::make_unique<TestResult>();
	req = RequestHelper::createFromUrl("https://example.com/2");
	req.handler = std::make_unique<StateHandler>(*result);
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("SD", result->states());
}
