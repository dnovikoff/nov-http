#include "tester.hpp"

#include <nov/http/headers.hpp>

BOOST_AUTO_TEST_CASE (get_chunk_delayed) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/range/20?chunk_size=5&duration=1");
	req.timeouts.setAllMs(2000);
	req.handler = std::make_unique<StateHandler>(*result);
//	req.handler.reset(new LogHandler);
	tester.fetcher.fetch(std::move(req));
	tester.io.run();

	BOOST_CHECK(!result->headers.count(NovHttp::Headers::ContentLength));
	BOOST_CHECK(result->headers.count(NovHttp::Headers::TransferEncoding));
	BOOST_REQUIRE_EQUAL(1u, result->headers[NovHttp::Headers::TransferEncoding].size());
	BOOST_CHECK_EQUAL("chunked", result->headers[NovHttp::Headers::TransferEncoding].back());
	BOOST_CHECK_EQUAL("abcdefghijklmnopqrst", result->body);
}

BOOST_AUTO_TEST_CASE (could_not_connect) {
	auto result = std::make_unique<TestResult>();

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org:12345/");
	req.timeouts.setAllMs(2000);
	req.timeouts.resolve = std::chrono::milliseconds(500);
	req.timeouts.connect = std::chrono::milliseconds(500);
	req.handler = std::make_unique<StateHandler>(*result);
	tester.fetcher.fetch(std::move(req));
	tester.io.run();

	BOOST_CHECK(result->failReason == NovHttp::FailReason::ConnectTimeout || result->failReason == NovHttp::FailReason::ConnectError);
}

BOOST_AUTO_TEST_CASE (reuse_closed_connection) {
	auto result = std::make_unique<TestResult>();

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/");
	req.timeouts.setAllMs(2000);
	req.handler = std::make_unique<StateHandler>(*result);
	result->fetch = tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL(200, result->statusCode);
	auto oldSocket = result->socket;
	auto socket = oldSocket.lock();
	BOOST_REQUIRE(socket);
	socket->close();
	result = std::make_unique<TestResult>();
	req = RequestHelper::createFromUrl("http://httpbin.org/");
	req.timeouts.setAllMs(2000);
	req.handler = std::make_unique<StateHandler>(*result);
	tester.io.reset();
	result->fetch = tester.fetcher.fetch(std::move(req));
	tester.io.run();

	BOOST_CHECK_EQUAL("SCSD", result->states());
	BOOST_CHECK_EQUAL(200, result->statusCode);
}

BOOST_AUTO_TEST_CASE (close_connection_and_reconnect) {
	auto result = std::make_unique<TestResult>();

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/get");
	req.timeouts.setAllMs(2000);
	req.handler = std::make_unique<StateHandler>(*result);
	req.headers[NovHttp::Headers::Connection] = {NovHttp::HeaderValues::ConnectionClose};
	result->fetch = tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCSD", result->states());
	result = std::make_unique<TestResult>();
	req = RequestHelper::createFromUrl("http://httpbin.org/get");
	req.timeouts.setAllMs(2000);
	req.handler = std::make_unique<StateHandler>(*result);
	tester.io.reset();
	result->fetch = tester.fetcher.fetch(std::move(req));
	tester.io.run();

	BOOST_CHECK_EQUAL("CSD", result->states());
}
