#include <thread>

#include <nov/http/socket_cache.hpp>

#include "test.hpp"

using MyCache = NovHttp::SocketCache<int>;

auto i = [] (int i) {
	return std::make_shared<int>(i);
};

BOOST_AUTO_TEST_CASE (cache_size) {
	MyCache cache(5, std::chrono::seconds(5));
	BOOST_REQUIRE_EQUAL(0, cache.size());
	cache.add("one", i(1));
	BOOST_REQUIRE_EQUAL(1, cache.size());
	auto r = cache.get("one");
	BOOST_REQUIRE_EQUAL(0, cache.size());
	cache.add("one", r);
	BOOST_REQUIRE_EQUAL(1, cache.size());
}

BOOST_AUTO_TEST_CASE (multiple_items) {
	MyCache cache(5, std::chrono::seconds(5));
	BOOST_REQUIRE_EQUAL(0, cache.size());
	cache.add("one", i(1));
	cache.add("one", i(2));
	cache.add("one", i(3));
	cache.add("one", i(4));
	BOOST_REQUIRE_EQUAL(4, cache.size());
	BOOST_REQUIRE_EQUAL(1, *cache.get("one"));
	BOOST_REQUIRE_EQUAL(2, *cache.get("one"));
	BOOST_REQUIRE_EQUAL(3, *cache.get("one"));
	BOOST_REQUIRE_EQUAL(4, *cache.get("one"));
}
