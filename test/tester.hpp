#pragma once

#include "test.hpp"

#include <nov/http/body_info.hpp>
#include <nov/http/fetch.hpp>
#include <nov/http/fetcher.hpp>
#include <nov/http/request_helper.hpp>
#include <nov/http/response_handler.hpp>
#include <nov/http/string_view.hpp>
#include <nov/http/make_unique.hpp>

namespace {

using NovHttp::Fetch;
using NovHttp::CStringView;
using NovHttp::ResponseHandler;
using NovHttp::BodyInfo;
using NovHttp::Request;
using NovHttp::FailReason;
using NovHttp::Fetcher;
using NovHttp::RequestHelper;

class LogHandler: public ResponseHandler {
public:
	void resolved() override {
		std::cout << "resolved()" << std::endl;
	}
	void connected() override {
		std::cout << "connected()" << std::endl;
	}
	void handshake() override {
		std::cout << "handshake()" << std::endl;
	}
	void started() override {
		std::cout << "started()" << std::endl;
	}
	void status(const CStringView& version, int code, const CStringView& reason) override {
		std::cout << "status(" << version << ", " << code << ", " << reason<< ")" << std::endl;
	}
	void header(const CStringView& key, const CStringView& value) override {
		std::cout << "header(" << key << ", " << value << ")" << std::endl;
	}
	bool body(const CStringView& chunk) override {
		std::cout << "chunk(" << chunk << ")" << std::endl;
		return true;
	}
	bool done() override {
		std::cout << "done()" << std::endl;
		return true;
	}
	void fail(FailReason reason, const std::string& message) override {
		std::cout << static_cast<int>(reason) << ": " + message << std::endl;
	}
	void headersDone(const BodyInfo& info) override {
		std::cout << "headersDone(body=" << info.expectBody << ", chunked=" << info.chunked << ", length=" << info.length << ")" << std::endl;
	}
	void headerValueNextLine(const CStringView& valueChunk) override {
		std::cout << "headerValueNextLine(" << valueChunk << ")" << std::endl;
	}
	~LogHandler() override {
		std::cout << "~LogHandler()" << std::endl;
	}
};

struct TestResult {
	std::ostringstream oss;
	int statusCode = -1;
	std::string statusReason;
	std::string statusVersion;
	NovHttp::Request::Headers headers;
	std::string failMessage;
	FailReason failReason;
	std::string body;
	BodyInfo info;
	std::weak_ptr<NovHttp::Socket> socket;
	std::weak_ptr<NovHttp::Fetch> fetch;

	std::string states() {
		return oss.str();
	}

	bool bodyContains(const std::string& sub) {
		if (body.find(sub) != std::string::npos) {
			return true;
		}
		std::cout << "Body not contains: " << sub << std::endl;
		std::cout << "Debug:" << std::endl << body << std::endl;
		return false;
	}
};

class StateHandler: public ResponseHandler {
public:
	explicit StateHandler(TestResult& out): result(out) {
	}

	void started() override {
		result.oss << "S";
	}
	void resolved() override {
		result.oss << "R";
	}
	void connected() override {
		result.oss << "C";
		auto locked = result.fetch.lock();
		if (locked) {
			result.socket = locked->request().socket;
		}
	}
	void handshake() override {
		result.oss << "H";
	}
	void status(const CStringView& r, int c, const CStringView& v) override {
		result.statusCode = c;
		result.statusReason = r.toString();
		result.statusVersion = v.toString();
	}
	void header(const CStringView& h, const CStringView& v) override {
		result.headers[h.toString()].push_back(v.toString());
	}
	bool body(const CStringView& chunk) override {
		result.body += chunk.toString();
		return true;
	}
	bool done() override {
		result.oss << "D";
		return true;
	}
	void fail(FailReason reason, const std::string& message) override {
		result.oss << "F";
		result.failMessage = message;
		result.failReason = reason;
		std::cout << static_cast<int>(reason) << ": " + message << std::endl;
	}
	void headersDone(const BodyInfo& info) override {
		result.info = info;
	}
	void headerValueNextLine(const CStringView&) override {
	}

private:
	TestResult& result;
};

class Tester {
public:
	Tester(): fetcher(io, 1024) {
		fetcher.setEndpointCache(5, std::chrono::seconds(5));
		fetcher.setSocketCache(5, std::chrono::seconds(5));
	}

	boost::asio::io_service io;
	Fetcher fetcher;
};

} // namespace
