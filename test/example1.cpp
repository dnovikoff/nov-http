#include "tester.hpp"

BOOST_AUTO_TEST_CASE (shared_for_https_http) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://example.com");
	req.timeouts.setAllMs(1000);
	req.handler = std::make_unique<StateHandler>(*result);
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("RCSD", result->states());

	result = std::make_unique<TestResult>();;
	req = RequestHelper::createFromUrl("https://example.com");
	req.timeouts.setAllMs(2000);
	req.handler = std::make_unique<StateHandler>(*result);
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
	BOOST_CHECK_EQUAL("CHSD", result->states());
}
