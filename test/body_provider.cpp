#include <nov/http/body_provider.hpp>
#include <nov/http/request_writer.hpp>

#include "test.hpp"

using NovHttp::BodyProvider;
using NovHttp::StringBodyProvider;
using NovHttp::UnknownBodyProvider;
using NovHttp::Buffer;
using NovHttp::RequestWriter;
using NovHttp::BodyWriter;

std::string toString(RequestWriter& rw) {
	std::string result;
	for (auto& x: rw.chain().chain()) {
		result += x->content().toString();
	}
	return result;
}

BOOST_AUTO_TEST_CASE (string) {
	auto body = StringBodyProvider::create("text body");
	BOOST_REQUIRE_EQUAL(9u, body->size());
	RequestWriter rw(NovHttp::BufferAllocator(1024));
	BodyWriter br(&rw);
	BOOST_REQUIRE(!body->more(br));
	BOOST_CHECK_EQUAL("text body", toString(rw));
}
