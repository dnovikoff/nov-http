#include "tester.hpp"

BOOST_AUTO_TEST_CASE (post_simple) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/post");
	req.timeouts.setAllMs(1000);
	req.method = "POST";
	req.handler = std::make_unique<StateHandler>(*result);
	req.handler.reset(new LogHandler);

	req.body = NovHttp::StringBodyProvider::create("Hello httpbin.org");
	tester.io.reset();
	tester.fetcher.fetch(std::move(req));
	tester.io.run();
}

BOOST_AUTO_TEST_CASE (post_chunked) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/post");
	req.timeouts.setAllMs(1000);
	req.method = "POST";
	req.handler = std::make_unique<StateHandler>(*result);
	req.handler.reset(new LogHandler);

	auto body = NovHttp::UnknownBodyProvider::create(&tester.io, 0);
	req.body = body;
	tester.io.reset();
	auto needToHoldFetch = tester.fetcher.fetch(std::move(req));
	NovHttp::Timer::start(tester.io, std::chrono::seconds(2), [body] (const boost::system::error_code& e) {
		if (e) {
			return;
		}
		body->add("Chunk1");
		std::cout << "Chunk 1 in the way" << std::endl;
	});
	NovHttp::Timer::start(tester.io, std::chrono::seconds(3), [body] (const boost::system::error_code& e) {
		if (e) {
			return;
		}
		body->add("ChunkOther");
		body->done();
		std::cout << "Chunk 2 in the way" << std::endl;
	});
	tester.io.run();
}

BOOST_AUTO_TEST_CASE (post_with_done_delayed) {
	auto result = std::make_unique<TestResult>();;

	Tester tester;
	auto req = RequestHelper::createFromUrl("http://httpbin.org/post");
	req.timeouts.setAllMs(2000);
	req.method = "POST";
	req.handler = std::make_unique<StateHandler>(*result);

	auto body = NovHttp::UnknownBodyProvider::create(&tester.io, 0);
	req.body = body;
	tester.io.reset();
	auto needToHoldFetch = tester.fetcher.fetch(std::move(req));
	auto* io = &tester.io;
	NovHttp::Timer::start(*io, std::chrono::seconds(1), [body, io] (const boost::system::error_code&) {
		body->add("Chunk1");
		NovHttp::Timer::start(*io, std::chrono::seconds(1), [body, io] (const boost::system::error_code&) {
			body->add("Chunk2");
			NovHttp::Timer::start(*io, std::chrono::seconds(1), [body, io] (const boost::system::error_code&) {
				body->done();
			});
		});
	});

	tester.io.run();
	BOOST_CHECK(result->bodyContains(R"=("data": "Chunk1Chunk2")="));
}
