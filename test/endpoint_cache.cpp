#include <thread>

#include <nov/http/endpoint_cache.hpp>

#include "test.hpp"

using MyCache = NovHttp::EndpointCache<std::string, int>;

auto i = [] (int i) {
	return std::make_shared<int>(i);
};

BOOST_AUTO_TEST_CASE (cache_size) {
	MyCache cache(5, std::chrono::seconds(5));
	BOOST_REQUIRE_EQUAL(0, cache.size());
	cache.set("one", i(1));
	BOOST_REQUIRE_EQUAL(1, cache.size());
	auto ptr = cache.get("one");
	BOOST_REQUIRE(cache.get("one"));
	BOOST_REQUIRE_EQUAL(1, *ptr);
	cache.set("one", i(11));
	BOOST_REQUIRE_EQUAL(1, cache.size());
	BOOST_REQUIRE_EQUAL(11, *cache.get("one"));
	cache.set("two", i(2));
	cache.set("3", i(3));
	cache.set("4", i(4));
	cache.set("5", i(5));
	BOOST_REQUIRE_EQUAL(5, cache.size());
	cache.set("6", i(6));
	BOOST_REQUIRE(!cache.get("one"));
	BOOST_REQUIRE(cache.get("two"));
	BOOST_REQUIRE(cache.get("6"));
	cache.set("1", i(2));
	cache.set("2", i(3));
	cache.set("3", i(4));
	cache.set("4", i(5));
	BOOST_REQUIRE(cache.get("6"));
}

BOOST_AUTO_TEST_CASE (cache_timeout) {
	MyCache cache(5, std::chrono::milliseconds(10));
	BOOST_REQUIRE_EQUAL(0, cache.size());
	cache.set("1", i(1));
	BOOST_REQUIRE_EQUAL(1, cache.size());
	BOOST_REQUIRE(cache.get("1"));
	std::this_thread::sleep_for(std::chrono::milliseconds(20));
	BOOST_REQUIRE_EQUAL(1, cache.size());
	BOOST_REQUIRE(!cache.get("1"));
	BOOST_REQUIRE_EQUAL(0, cache.size());
}
