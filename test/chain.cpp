#include "test.hpp"

#include <nov/http/buffer_allocator.hpp>
#include <nov/http/buffer_chain.hpp>

using NovHttp::Buffer;
using NovHttp::BufferChain;
using NovHttp::BufferAllocator;

BOOST_AUTO_TEST_CASE (simple) {
	BufferChain chain(BufferAllocator(10));
	chain.write("First. ");
	chain.write("And some other big data");
	BOOST_REQUIRE_EQUAL(2u, chain.chain().size());
	BOOST_REQUIRE_EQUAL("First. And", chain.chain().front()->content().toString());
	BOOST_REQUIRE_EQUAL(" some other big data", chain.chain().back()->content().toString());

	BOOST_REQUIRE_EQUAL("First. And some other big data", chain.toString());
	chain.readCompleted(2);
	BOOST_REQUIRE_EQUAL(2u, chain.chain().size());
	BOOST_REQUIRE_EQUAL("rst. And some other big data", chain.toString());
	chain.readCompleted(12);
	BOOST_REQUIRE_EQUAL(1u, chain.chain().size());
	BOOST_REQUIRE_EQUAL("e other big data", chain.toString());
}
