#include "test.hpp"

#include <nov/http/buffer_allocator.hpp>
#include <nov/http/buffer_chain_to_asio.hpp>
#include <nov/http/request_writer.hpp>

using NovHttp::Buffer;
using NovHttp::BufferAllocator;
using NovHttp::RequestWriter;

static void writeBody(RequestWriter& w, NovHttp::CStringView view) {
	w.writeChunkSize(view.size);
	w.writeBody(view);
	w.writeChunkEnd();
}

BOOST_AUTO_TEST_CASE (simple) {
	RequestWriter w(BufferAllocator(1024));
	w.writeRequest("GET", "/");
	w.writeHeader("Content-Length", "100");
	w.writeHeaderEnd();
	auto e = "GET / HTTP/1.1\r\nContent-Length: 100\r\n\r\n";
	BOOST_CHECK_EQUAL(e , w.chain().toString());
}

BOOST_AUTO_TEST_CASE (body) {
	RequestWriter w(BufferAllocator(1024));
	w.writeRequest("GET", "/");
	w.writeHeaderEnd();
	w.writeBody("Content");
	auto e = "GET / HTTP/1.1\r\n\r\nContent";
	BOOST_CHECK_EQUAL(e , w.chain().toString());
}

BOOST_AUTO_TEST_CASE (body_chunk1) {
	RequestWriter w(BufferAllocator(1024));
	w.writeRequest("GET", "/");
	w.writeHeaderEnd();
	writeBody(w, "Content");
	w.writeBodyEnd();
	auto e = "GET / HTTP/1.1\r\n\r\n7\r\nContent\r\n0\r\n\r\n";
	BOOST_CHECK_EQUAL(e , w.chain().toString());
}

BOOST_AUTO_TEST_CASE (body_chunk2) {
	RequestWriter w(BufferAllocator(1024));
	w.writeRequest("GET", "/");
	w.writeHeaderEnd();
	writeBody(w, "More");
	writeBody(w, "Chunks");
	w.writeBodyEnd();
	auto e = "GET / HTTP/1.1\r\n\r\n4\r\nMore\r\n6\r\nChunks\r\n0\r\n\r\n";
	BOOST_CHECK_EQUAL(e , w.chain().toString());
}
