#include <nov/http/string_view.hpp>

#include "test.hpp"

using NovHttp::CStringView;

BOOST_AUTO_TEST_CASE (equals) {
	CStringView t("Test");
	BOOST_REQUIRE(t.equals("Test"));
	BOOST_REQUIRE(!t.equals("Test "));
	BOOST_REQUIRE(!t.equals("test"));
	BOOST_REQUIRE(!t.iEquals("tEst "));
	BOOST_REQUIRE(t.iEquals("tEst"));
}
