#include <nov/http/buffer.hpp>

#include "test.hpp"

using NovHttp::Buffer;
using NovHttp::StringView;

BOOST_AUTO_TEST_CASE (broken) {
	char mem[] = "just string";
	Buffer buf(mem);
	buf.add(sizeof(mem));
	auto s = buf.until(':');
	BOOST_REQUIRE(!s.isValid());
}

template<std::size_t Size>
static inline void twrite(Buffer& b, const char(&x)[Size]) {
	memcpy(b.buffer().pointer, x, Size-1);
	b.add(Size-1);
}

BOOST_AUTO_TEST_CASE (first_ok) {
	char mem[] = "just string";
	Buffer buf(mem);
	BOOST_REQUIRE_EQUAL(sizeof(mem), buf.buffer().size);
	buf.add(buf.buffer().size);
	auto s = buf.until(' ');
	BOOST_REQUIRE(s.isValid());
	BOOST_REQUIRE_EQUAL("just", s.toString());
	s = buf.until(' ');
	BOOST_REQUIRE(!s.isValid());
	twrite(buf, "just1");
	s = buf.until(' ');
	BOOST_REQUIRE(!s.isValid());
}

BOOST_AUTO_TEST_CASE (second_ok) {
	char mem[] = "just another string";
	Buffer buf(mem);
	buf.add(sizeof(mem));
	auto s = buf.until(' ');
	BOOST_REQUIRE_EQUAL("just", s.toString());
	s = buf.until(' ');
	BOOST_REQUIRE_EQUAL("another", s.toString());
	s = buf.until(' ');
	BOOST_REQUIRE(!s.isValid());
}

BOOST_AUTO_TEST_CASE (out_of_buffer) {
	char mem[] = "just another string";
	StringView view(mem, sizeof("just another")-1);
	Buffer buf(view);
	buf.add(view.size);
	auto s = buf.until(' ');
	BOOST_REQUIRE_EQUAL("just", s.toString());
	s = buf.until(' ');
	BOOST_REQUIRE(!s.isValid());
}

BOOST_AUTO_TEST_CASE (fill) {
	char mem[128];
	Buffer buf(mem);
	twrite(buf, "Key: value\nKey1: another");
	BOOST_REQUIRE_EQUAL("Key", buf.until(':').toString());
	BOOST_REQUIRE_EQUAL(" value", buf.until('\n').toString());
	BOOST_REQUIRE_EQUAL("Key1", buf.until(':').toString());
	BOOST_REQUIRE(!buf.until('\n').isValid());
	twrite(buf, " value\n");
	auto s = buf.until('\n');
	BOOST_REQUIRE(s.isValid());
	BOOST_REQUIRE_EQUAL(" another value", s.toString());
}

BOOST_AUTO_TEST_CASE (atLeast) {
	char mem[128];
	Buffer buf(mem);
	BOOST_REQUIRE_EQUAL(128, buf.buffer().size);
	BOOST_REQUIRE(buf.buffer(128).isValid());
	BOOST_REQUIRE(!buf.buffer(129).isValid());
	twrite(buf, "hello!");
	BOOST_REQUIRE_EQUAL(128 - sizeof("hello"), buf.buffer().size);
	BOOST_REQUIRE(!buf.buffer(128).isValid());
	BOOST_REQUIRE_EQUAL("hello", buf.until('!').toString());
	BOOST_REQUIRE(buf.buffer(128).isValid());
	BOOST_REQUIRE(!buf.buffer(129).isValid());
}

BOOST_AUTO_TEST_CASE (read2) {
	char mem[128];
	Buffer buf(mem);
	twrite(buf, "First\nsecond\rthrird\nand more\n");
	BOOST_REQUIRE_EQUAL("First", buf.until('\n').toString());
	BOOST_REQUIRE_EQUAL("second\rthrird", buf.until('\n').toString());
	BOOST_REQUIRE_EQUAL("and more", buf.until('\n').toString());
}

BOOST_AUTO_TEST_CASE (checkEmpty) {
	char mem[128];
	Buffer buf(mem);
	BOOST_REQUIRE(buf.isEmpty());
	twrite(buf, "One!Two!");
	BOOST_REQUIRE(!buf.isEmpty());
	BOOST_REQUIRE_EQUAL("One", buf.until('!').toString());
	BOOST_REQUIRE(!buf.isEmpty());
	BOOST_REQUIRE_EQUAL("Two", buf.until('!').toString());
	BOOST_REQUIRE(buf.isEmpty());
}

BOOST_AUTO_TEST_CASE (tail) {
	char mem[128];
	Buffer buf(mem);
	BOOST_REQUIRE(buf.isEmpty());
	twrite(buf, "One!Two!");
	BOOST_REQUIRE_EQUAL("One!Two!", buf.tail().toString());
	BOOST_REQUIRE(buf.isEmpty());
	twrite(buf, "One!Two!");
	BOOST_REQUIRE_EQUAL("One", buf.until('!').toString());
	BOOST_REQUIRE_EQUAL("Two!", buf.tail().toString());
}
