#include "test.hpp"

#include <nov/http/timer.hpp>

BOOST_AUTO_TEST_CASE (timer) {
	boost::asio::io_service io;
	auto holder = NovHttp::Timer::start(io, std::chrono::seconds(1), [] (const boost::system::error_code&) {});
	io.run();
	BOOST_CHECK(true);
}
