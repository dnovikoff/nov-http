file(GLOB files "*.cpp")

set(TEST_LIST)

foreach(file ${files})
	get_filename_component(test ${file} NAME_WE)
	set(test ${test}_test)
	add_executable(${test} ${file})
	target_link_libraries( ${test} ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY} nov_http pthread)
	set(passed "${test}.passed")

	add_custom_command(
		OUTPUT ${passed}
		COMMAND $<TARGET_FILE:${test}> && touch ${passed}
		#COMMAND /usr/bin/valgrind $<TARGET_FILE:${test}> && touch ${passed}
		DEPENDS ${test}
	)

	set(TEST_LIST ${TEST_LIST} ${passed})
endforeach()

add_custom_target(run_all_tests ALL DEPENDS ${TEST_LIST})