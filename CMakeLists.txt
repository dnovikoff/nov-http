cmake_minimum_required(VERSION 2.8)

project(nov-http)

find_package( Boost 1.49 COMPONENTS unit_test_framework system iostreams regex REQUIRED )
include_directories(${CMAKE_SOURCE_DIR}/src ${CMAKE_SOURCE_DIR} ${Boost_INCLUDE_DIRS})

# For windows builds. It also ok for linux.
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

add_definitions(-std=c++11 -Wall -pedantic -Werror -Wextra -g)

add_subdirectory(src/nov/http)
add_subdirectory(test)
