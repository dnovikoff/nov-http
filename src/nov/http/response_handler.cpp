#include "response_handler.hpp"

namespace NovHttp {

const char* enumValue(FailReason reason) {
#define TMP(X) case FailReason::X: return #X;
	switch(reason) {
	TMP(ResolveError)
	TMP(ConnectError)
	TMP(ParseError)
	TMP(SendError)
	TMP(ReadError)
	TMP(Canceled)
	TMP(SslHandshakeError)
	TMP(ResolveTimeout)
	TMP(ConnectTimeout)
	TMP(HandshakeTimeout)
	TMP(ReadTimeout)
	TMP(WriteTimeout)
	TMP(Timeout)
	TMP(DecodeError)
	TMP(TooMuchRedirect)
	}
#undef TMP
	return "Unknown";
}

ResponseHandler::~ResponseHandler() {}

} // namespace NovHttp
