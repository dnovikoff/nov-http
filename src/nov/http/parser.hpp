#pragma once

#include <nov/http/buffer.hpp>
#include <nov/http/string_view.hpp>

namespace NovHttp {

class Parser {
public:
	enum State {
		StatusLine,
		Header,
		HeaderMore,
		Body,
		ChunkStart,
		ChunkMiddle,
		Failed,
		Pause,
		Done,
	};

	Parser() {}

	inline void setBuffer(Buffer& buffer) {
		buffer_ = &buffer;
	}

	State parseStatus(CStringView& version, int& code, CStringView& reason);

	State parseHeader(CStringView& key, CStringView& value);
	State parseBody(std::size_t len, CStringView& chunk);
	State parseChunkMiddle(std::size_t len, CStringView& chunk);
	State parseChunkHeader(std::size_t& len);

	static bool parseLength(const CStringView& x, std::size_t& result);
	static bool parseHex(const StringView& x, std::size_t& result) ;

private:
	static constexpr std::initializer_list<char> spaceChars() {
		return {'\t',' '};
	}

	static inline Buffer bufferLine(const StringView& line) {
		Buffer b(line);
		b.add(line.size);
		return b;
	}

	static inline StringView untilSpace(Buffer& buffer) {
		auto r = buffer.untilOneOf<StringView>(spaceChars());
		skipSpaces(buffer);
		return r;
	}
	static inline std::size_t skipSpaces(Buffer& buffer) {
		return buffer.skipAnyOf(spaceChars());
	}

	inline Buffer& buffer() {
		return *buffer_;
	}

	StringView next();

	Buffer* buffer_ = nullptr;
};

} // namespace NovHttp
