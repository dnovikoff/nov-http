#pragma once

#include <vector>

#include <boost/asio.hpp>

#include <nov/http/buffer_chain.hpp>

namespace NovHttp {

inline std::vector<boost::asio::const_buffer> toAsio(const BufferChain& x) {
	std::vector<boost::asio::const_buffer> ret;
	auto& chain = x.chain();
	ret.reserve(chain.size());
	for (auto& b: chain) {
		auto content = b->content();
		ret.emplace_back(boost::asio::const_buffer(content.pointer, content.size));
	}
	return ret;
}

} // namespace NovHttp
