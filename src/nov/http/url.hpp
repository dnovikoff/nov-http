#pragma once

#include <string>

namespace NovHttp {

class Url {
public:
	Url() {}

	const std::string& path() const {
		return path_;
	}

	const std::string& host() const {
		return host_;
	}

	bool isSsl() const {
		return isSsl_;
	}

	bool isIp() const {
		return isIp_;
	}

	bool isValid() const {
		return isValid_;
	}

	int port() const {
		return port_;
	}

	std::string id() const {
		return id_;
	}

	const std::string& portStr() const {
		return portStr_;
	}

	const std::string& hostHeader() const {
		return hostHeader_;
	}

	void setPath(const std::string& path);
	bool parse(const std::string& url);

	static Url create(const std::string& url);

private:
	std::string id_;
	std::string path_;
	std::string host_;
	int port_ = 0;
	std::string portStr_;
	std::string hostHeader_;
	bool isSsl_ = 0;
	bool isIp_ = false;
	bool isValid_ = false;
};

} // namespace NovHttp
