#include "fetch.hpp"
#include <boost/asio/ssl.hpp>

#define DEBUG_ACTION
//#include <iostream>
//#define DEBUG_ACTION std::cout << "ACTION: " << __LINE__ << std::endl;

#include <nov/http/buffer_chain_to_asio.hpp>
#include <nov/http/fetcher.hpp>
#include <nov/http/filter_response_decorator.hpp>
#include <nov/http/headers.hpp>

namespace NovHttp {

using boost::asio::async_read;
using boost::asio::async_write;

#define IO_HOLDER() std::make_tuple(shared_from_this(),  request_.socket)

std::shared_ptr<Fetch> Fetch::create(Fetcher* fetcher, Request&& request) {
	return std::make_shared<Fetch>(fetcher, std::move(request));
}

Fetch::Fetch(Fetcher* fetcher, Request&& r):
	fetcher_(fetcher),
	request_(std::move(r)),
	decorator_(request_.handler.get()),
	parser_(&decorator_, r.expectBody()),
	writer_(fetcher->allocator()),
	body_(&writer_)
{}

void Fetch::fetch() {
	if (state_ != NotStarted) {
		return;
	}
	init();
}

void Fetch::cancelImpl() {
	if (state_ != Started) {
		return;
	}
	fail(FailReason::Canceled, "Canceled");
	state_ = Canceled;
}

void Fetch::cancel() {
	auto holder = shared_from_this();
	fetcher_->io().post([holder] {
		holder->cancelImpl();
	});
}

void Fetch::start() {
	if (isFinished()) {
		return;
	}
	handler().started();
	writeRequest();
	afterWrite();
	buffer_ = fetcher_->allocator().allocate(1024);
	afterParsed();
}

void Fetch::handshake() {
	if (isFinished()) {
		return;
	}
	auto holder = IO_HOLDER();
	startTimer(phaseTimer_, request_.timeouts.handshake, FailReason::HandshakeTimeout);
	request_.socket->handshake([this, holder] (const boost::system::error_code& err) {
		DEBUG_ACTION;
		if (err) {
			fail(FailReason::SslHandshakeError, err.message());
			return;
		}
		handler().handshake();
		start();
	});
}

void Fetch::beforeConnect() {
	if (isSsl) {
		request_.socket = std::make_shared<SslSocket>(fetcher_->io(), fetcher_->sslContext());
	} else {
		request_.socket = std::make_shared<TcpSocket>(fetcher_->io());
	}
	startTimer(phaseTimer_, request_.timeouts.connect, FailReason::ConnectTimeout);
}

void Fetch::onConnected() {
	handler().connected();
	if (request_.url.isSsl()) {
		return handshake();
	}
	start();
}

void Fetch::connect(Socket::Endpoint endpoint) {
	if (isFinished()) {
		return;
	}
	beforeConnect();
	auto holder = IO_HOLDER();
	request_.socket->connect(endpoint, [this, holder] (const boost::system::error_code& err) {
		DEBUG_ACTION;
		if (state_ != Started) {
			return;
		}
		if (err) {
			return fail(FailReason::ConnectError, err.message());
		}
		onConnected();
	});
}

void Fetch::connect(Resolver::iterator iterator) {
	if (isFinished()) {
		return;
	}
	beforeConnect();

	auto holder = IO_HOLDER();
	request_.socket->connect(iterator, [this, holder] (const boost::system::error_code& err, Resolver::iterator iter) {
		DEBUG_ACTION;
		if (state_ != Started) {
			return;
		}
		if (err) {
			return fail(FailReason::ConnectError, err.message());
		}
		if (iter == Resolver::iterator()) {
			return fail(FailReason::ResolveError, err.message());
		}
		request_.endopoint = std::make_shared<Socket::Endpoint>(*iter);
		fetcher_->cacheEndpoint(request_);
		onConnected();
	});
}

void Fetch::init() {
	state_ = Started;

	isSsl = request_.url.isSsl();

	startTimer(totalTimer_, request_.timeouts.total, FailReason::Timeout);

	if (request_.socket) {
		connectionReused_ = true;
		request_.socket->cancel();
		return start();
	}
	if (!request_.endopoint) {
		return resolve();
	}
	connect(*request_.endopoint);
}

void Fetch::resolve() {
	if (state_ != Started) {
		return;
	}
	Resolver::query query(request_.url.host(), request_.url.portStr());
	resolver_ = std::make_shared<Socket::Resolver>(fetcher_->io());
	startTimer(phaseTimer_, request_.timeouts.resolve, FailReason::ResolveTimeout);
	auto holder = std::make_tuple(shared_from_this(), resolver_);
	resolver_->async_resolve(query, [this, holder] (const boost::system::error_code& err,
		Resolver::iterator iter) {
		DEBUG_ACTION;
		resolver_ = nullptr;
		if (err) {
			return fail(FailReason::ResolveError, err.message());
		}
		handler().resolved();
		connect(iter);
	});
}

void Fetch::close() {
	if (!request_.socket) {
		return;
	}
	request_.socket->close();
	request_.socket = nullptr;
}

void Fetch::fail(FailReason reason, const std::string& message) {
	if (state_ != Started && state_ != Paused) {
		return;
	}
	state_ = Failed;

	handler().fail(reason, message);

	if (resolver_) {
		resolver_->cancel();
		resolver_ = nullptr;
	}

	close();
	cancelTimers();
	handler().finalize();
}

CStringView Fetch::viewFromString(const std::string& x) {
	return CStringView(x.c_str(), x.length());
}

void Fetch::writeRequest() {
	// Caused by reinit
	if (!writer_.empty()) {
		return;
	}
	writer_.writeRequest(viewFromString(request_.method), viewFromString(request_.url.path()));
	if (request_.body) {
		request_.body->start(this);
		auto size = request_.body->size();
		if (size) {
			request_.headers[Headers::ContentLength] = {std::to_string(size)};
			request_.headers.erase(Headers::TransferEncoding);
		} else {
			// manualy set Content-Length is ok for TransferEncoding=chunk (chunk will be used)
			request_.headers[Headers::TransferEncoding] = {HeaderValues::TransferEncodingChunked};
		}
		moreBodyExpected_ = true;
	}

	for (auto& p: request_.headers) {
		if (p.second.empty()) {
			continue;
		}
		if (request_.useCommaForMultipleHeaders) {
			writer_.writeHeader(viewFromString(p.first), viewFromString(boost::join(p.second, ", ")));
		} else {
			for (auto& x: p.second) {
				writer_.writeHeader(viewFromString(p.first), viewFromString(x));
			}
		}
	}
	writer_.writeHeaderEnd();
	readBodyData();
}

void Fetch::afterWrite() {
	if (isFinished() || writer_.empty()) {
		return;
	}
	state_ = Started;
	startTimer(phaseTimer_, request_.timeouts.idle, FailReason::WriteTimeout);
	auto holder = IO_HOLDER();
	request_.socket->write(writer_.chain(), [this, holder] (const boost::system::error_code& e, std::size_t transferred) {
		DEBUG_ACTION;
		auto resuedFlag = connectionReused_;
		connectionReused_ = false;
		if (e) {
			if (resuedFlag) {
				request_.socket = nullptr;
				init();
				return;
			}
			return fail(FailReason::SendError, e.message());
		}
		cancelTimer(phaseTimer_);
		if (writer_.readCompleted(transferred)) {
			DEBUG_ACTION;
			return afterWrite();
		}
		if (!moreBodyExpected_) {
			DEBUG_ACTION;
			// TODO: close one half of the pipe if close
			writeDone_ = true;
			if (state_ == Started) {
				// Now wait for read
				startTimer(phaseTimer_, request_.timeouts.idle, FailReason::ReadTimeout);
			}
			return;
		}
		state_ = Paused;
		DEBUG_ACTION;
		resumeBodySendImpl();
		return;
	});
}

void Fetch::afterParsed() {
	if (isFinished()) {
		return;
	}
	auto holder = IO_HOLDER();
	// TODO: read timeout to be started only after full write
	startTimer(phaseTimer_, request_.timeouts.idle, FailReason::ReadTimeout);
	auto socket = request_.socket;
	socket->read(buffer_->buffer(), [this, holder, socket] (const boost::system::error_code& e, std::size_t transferred) {
		DEBUG_ACTION;
		if (e) {
			// Could be second attempt
			if (socket == request_.socket) {
				fail(FailReason::ReadError, e.message());
			}
			return;
		}
		cancelTimer(phaseTimer_);
		buffer_->add(transferred);
		onRead();
	});
}

void Fetch::cancelTimers() {
	cancelTimer(phaseTimer_);
	cancelTimer(totalTimer_);
}

void Fetch::done() {
	if (isFinished()) {
		return;
	}
	state_ = Done;
	cancelTimers();

	if (request_.connectionClose || decorator_.connectionClose()) {
		close();
	} else if (request_.socket) {
		request_.socket->cancel();
		fetcher_->reuseSocket(request_);
		request_.socket = nullptr;
	}
	request_.handler->finalize();
}

void Fetch::onRead() {
	auto state = parser_.parse(*buffer_);
	if (state == Parser::Failed) {
		// Error already send to handler
		return close();
	} else if (state == Parser::Done) {
		return done();
	} else if (state != Parser::Pause) {
		return;
	}
	if (!buffer_->cleanup()) {
		return fail(FailReason::ParseError, "Buffer is too small to read");
	}
	afterParsed();
}

void Fetch::startTimer(std::weak_ptr<Timer>& timer, Timer::Duration dur, FailReason reason) {
	cancelTimer(timer);
	if (dur == Timeouts::zero()) {
		return;
	}
	if (state_ == Failed) {
		return;
	}
	auto holder = shared_from_this();
	timer = Timer::start(fetcher_->io(), dur, [dur, reason, this, holder] (const boost::system::error_code& e) {
		DEBUG_ACTION;
		if (e || state_ != Started) {
			return;
		}
		fail(reason, enumValue(reason));
	});
}

void Fetch::cancelTimer(std::weak_ptr<Timer>& timer) {
	auto locked = timer.lock();
	if (!locked) {
		return;
	}
	locked->cancel();
	timer.reset();
}

void Fetch::resumeBodySend() {
	auto holder = shared_from_this();
	fetcher_->io().post([holder] {
		DEBUG_ACTION;
		holder->resumeBodySendImpl();
	});
}

void Fetch::resumeBodySendImpl() {
	if (state_ != Paused) {
		return;
	}
	readBodyData();
	afterWrite();
}

bool Fetch::readBodyData() {
	if (moreBodyExpected_) {
		moreBodyExpected_ = request_.body->more(body_);
	}

	return moreBodyExpected_;
}

bool Fetch::isFinished() const {
	return state_ >= Done;
}

} // namespace NovHttp
