#pragma once

#include <chrono>
#include <functional>
#include <memory>

#include <boost/asio/steady_timer.hpp>

namespace NovHttp {

class Timer: public std::enable_shared_from_this<Timer> {
public:
	using Handler = std::function<void(const boost::system::error_code&)>;
	using Duration = std::chrono::milliseconds;

	Timer(boost::asio::io_service& io, Duration dur, Handler h);
	~Timer();

	void cancel();

	static std::weak_ptr<Timer> start(boost::asio::io_service& io, Duration dur, Handler h);

private:
	void start();

	boost::asio::steady_timer timer_;
	Duration duration_;
	Handler handler_;
};

} // namespace NovHttp
