#pragma once

#include <nov/http/response_decorator.hpp>

namespace NovHttp {

class FetchResponseDecorator: public ResponseDecorator {
public:
	using OnHeadersDoneHandler = std::function<void(void)>;

	explicit FetchResponseDecorator(ResponseHandler* handler): ResponseDecorator(handler) {
	}

	void header(const CStringView& key, const CStringView& value) override;

	bool connectionClose() const {
		return connectionClose_;
	}

private:
	bool connectionClose_ = false;
};

} // namespace NovHttp
