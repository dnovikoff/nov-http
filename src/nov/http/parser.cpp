#include "parser.hpp"

namespace NovHttp {

Parser::State Parser::parseStatus(CStringView& version, int& code, CStringView& reason) {
	auto line = next();
	if (!line.isValid()) {
		return Pause;
	}
	auto tmp = bufferLine(line);
	version = untilSpace(tmp);
	if (!version.isValid()) {
		return Failed;
	}
	auto codeStr = tmp.tryRead(3);
	if (!codeStr.isValid()) {
		return Failed;
	}
	if (codeStr.pointer[0] < '1' || codeStr.pointer[0] > '5') {
		return Failed;
	}
	if (!std::isdigit(codeStr.pointer[1]) || !std::isdigit(codeStr.pointer[2])) {
		return Failed;
	}
	code = 0;
	for (auto i = 0; i < 3; ++i) {
		code = code*10 + (codeStr.pointer[i] - '0');
	}
	if (code == 0) {
		return Failed;
	}
	if (skipSpaces(tmp) < 1) {
		return Failed;
	}
	reason = tmp.tail();
	return Header;
}

Parser::State Parser::parseHeader(CStringView& key, CStringView& value) {
	auto line = next();
	if (!line.isValid()) {
		return Pause;
	}
	if (line.size == 0) {
		return Body;
	}
	auto tmp = bufferLine(line);
	if (tmp.skipAnyOf(spaceChars()) > 0) {
		value = tmp.tail();
		return HeaderMore;
	}

	key = tmp.until(':');
	if (!key.isValid()) {
		return Failed;
	}
	skipSpaces(tmp);
	value = tmp.tail();
	return Header;
}


Parser::State Parser::parseBody(std::size_t len, CStringView& chunk) {
	chunk = buffer().read(len);
	if (chunk.size < len) {
		return Pause;
	}
	return Done;
}

Parser::State Parser::parseChunkMiddle(std::size_t len, CStringView& chunk) {
	chunk = buffer().read(len);
	if (chunk.size < len) {
		return Pause;
	}
	auto tmp = buffer().tryRead(1);
	if (!tmp.isValid()) {
		return Pause;
	}
	if (tmp.equals("\r")) {
		tmp = buffer().tryRead(1);
		if (!tmp.isValid()) {
			return Pause;
		}
	}
	if (!tmp.equals("\n")) {
		return Failed;
	}
	return ChunkStart;
}

Parser::State Parser::parseChunkHeader(std::size_t& len) {
	auto lenStr = next();
	if (!lenStr.isValid()) {
		return Pause;
	}
	if (!parseHex(lenStr, len)) {
		return Failed;
	}
	return ChunkMiddle;
}

bool Parser::parseLength(const CStringView& x, std::size_t& result) {
	if (x.size == 0) {
		return false;
	}
	std::size_t r = 0;
	std::size_t prev = 0;
	for (auto i = x.pointer; i < x.pointer + x.size; ++i) {
		auto v = *i;
		if (v >= '0' && v <= '9') {
			v -= '0';
		} else {
			return false;
		}
		r = r*10 + v;
		// Overflow
		if (r < prev) {
			return false;
		}
		prev = r;
	}
	result = r;
	return true;
}

bool Parser::parseHex(const StringView& x, std::size_t& result) {
	if (x.size == 0) {
		return false;
	}
	std::size_t r = 0;
	for (auto i = x.pointer; i < x.pointer + x.size; ++i) {
		auto v = *i;
		if (v >= '0' && v <= '9') {
			v -= '0';
		} else if (v >= 'a' && v <= 'f') {
			v -= 'a' - 10;
		} else if (v >= 'A' && v <= 'F') {
			v -= 'A' - 10;
		} else {
			return false;
		}
		r = r*16 + v;
	}
	result = r;
	return true;
}

 StringView Parser::next() {
	auto r = buffer().until<StringView>('\n');
	if (r.isValid()) {
		if (r.size > 0 && r.pointer[r.size-1] == '\r') {
			--r.size;
		}
	}
	return r;
}

} // namespace NovHttp
