#pragma once

#include <atomic>
#include <memory>

#include <boost/asio.hpp>

#include <nov/http/request.hpp>
#include <nov/http/buffer_allocator.hpp>
#include <nov/http/endpoint_cache.hpp>
#include <nov/http/socket_cache.hpp>
#include <nov/http/make_unique.hpp>

namespace NovHttp {

class Fetch;

class Fetcher {
public:
	friend class Fetch;

	using Resolver = Socket::Resolver;
	using Io = boost::asio::io_service;
	using SslContext = boost::asio::ssl::context;
	using EndpointCacheType = EndpointCache<std::string, Socket::Endpoint>;
	using SocketCacheType = SocketCache<Socket>;
	using Duration = std::chrono::milliseconds;

	explicit Fetcher(boost::asio::io_service& io, BufferAllocator allocator);
	explicit Fetcher(boost::asio::io_service& io, std::size_t minBufferSize);

	Io& io() {
		return io_;
	}

	BufferAllocator& allocator() {
		return allocator_;
	}

	SslContext& sslContext() {
		return sslContext_;
	}

	void setEndpointCache(std::size_t max, Duration time) {
		endpointCache_ = std::make_unique<EndpointCacheType>(max, time);
	}

	void setSocketCache(std::size_t max, Duration time) {
		socketCache_ = std::make_unique<SocketCacheType>(max, time);
	}

	// Safe to call from other thread
	std::shared_ptr<Fetch> fetch(Request&&);

	void stop () {
		stopped_ = true;
	}

private:
	void fetchImpl(const std::shared_ptr<Fetch>& fetch);
	void cacheEndpoint(const Request&);
	void reuseSocket(const Request&);

	Io& io_;
	BufferAllocator allocator_;
	SslContext sslContext_;
	std::unique_ptr<EndpointCacheType> endpointCache_;
	std::unique_ptr<SocketCacheType> socketCache_;
	std::atomic_bool stopped_;
};


} // namespace NovHttp
