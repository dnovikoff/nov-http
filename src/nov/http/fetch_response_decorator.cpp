#include "fetch_response_decorator.hpp"

#include <nov/http/headers.hpp>

namespace NovHttp {

void FetchResponseDecorator::header(const CStringView& key, const CStringView& value) {
	if (key.iEquals(Headers::Connection)) {
		if (value.iEquals(HeaderValues::ConnectionClose)) {
			connectionClose_ = true;
		}
	}
	handler_->header(key, value);
}

} // namespace NovHttp
