#pragma once

#include <functional>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include <nov/http/buffer_chain.hpp>
#include <nov/http/buffer_chain_to_asio.hpp>
#include <nov/http/string_view.hpp>

namespace NovHttp {

class Socket {
public:
	using Resolver = boost::asio::ip::tcp::resolver;
	using Handler = std::function<void(const boost::system::error_code& e, std::size_t transferred)>;
	using ErrorHandler = std::function<void(const boost::system::error_code& e)>;
	using ConnectHandler = std::function<void(const boost::system::error_code& e, Resolver::iterator)>;
	using Endpoint = typename Resolver::endpoint_type;

	virtual ~Socket() {}

	virtual void write(const BufferChain& x, Handler handler) = 0;
	virtual void read(StringView x, Handler handler) = 0;
	virtual void handshake(ErrorHandler) {}
	virtual void connect(Resolver::iterator it, ConnectHandler handler) = 0;
	virtual void connect(const Endpoint& endpoint, ErrorHandler handler) = 0;
	virtual void close() = 0;
	virtual void cancel() = 0;
};

template<typename T>
class SocketBase: public Socket {
public:
	using SocketType = T;

	template<typename... Args>
	explicit SocketBase(Args&&... args): socket_(std::forward<Args>(args)...) {
	}

	void write(const BufferChain& x, Handler handler) override {
		socket_.async_write_some(toAsio(x), std::move(handler));
	}

	void read(StringView buf, Handler handler) override {
		socket_.async_read_some(boost::asio::buffer(buf.pointer, buf.size), std::move(handler));
	}

	void close() override {
		socket_.lowest_layer().close();
	}

	void cancel() override {
		boost::system::error_code unused;
		socket_.lowest_layer().cancel(unused);
	}

	void connect(Resolver::iterator it, ConnectHandler handler) override {
		boost::asio::async_connect(socket_.lowest_layer(), it, Resolver::iterator(), handler);
	}

	void connect(const Endpoint& endpoint, ErrorHandler handler) {
		socket_.lowest_layer().async_connect(endpoint, handler);
	}

protected:
	T socket_;
};

using TcpSocket = SocketBase<boost::asio::ip::tcp::socket>;
using SslImpl = boost::asio::ssl::stream<boost::asio::ip::tcp::socket>;

class SslSocket: public SocketBase<SslImpl> {
public:
	template<typename... Args>
	explicit SslSocket(Args&&... args): SocketBase<SslImpl>(std::forward<Args>(args)...) {
	}
	void handshake(ErrorHandler handler) override {
		socket_.async_handshake(boost::asio::ssl::stream_base::client, std::move(handler));
	}
};

} // namespace NovHttpm
