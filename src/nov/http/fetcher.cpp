#include "fetcher.hpp"

#include <nov/http/fetch.hpp>

namespace NovHttp {

Fetcher::Fetcher(boost::asio::io_service& io, BufferAllocator allocator):
	io_(io), allocator_(allocator),
	sslContext_(boost::asio::ssl::context::sslv23) {
	stopped_ = false;
	sslContext_.set_verify_mode(boost::asio::ssl::verify_none);
}

Fetcher::Fetcher(boost::asio::io_service& io, std::size_t minBufferSize): Fetcher(io, BufferAllocator(minBufferSize)) {
	stopped_ = false;
}

void Fetcher::fetchImpl(const std::shared_ptr<Fetch>& fetch) {
	auto& request = fetch->request();
	if (socketCache_ && !request.socket) {
		request.socket = socketCache_->get(request.url.id());
	}

	if (endpointCache_ && !request.endopoint) {
		auto cached = endpointCache_->get(request.url.host());
		if (cached) {
			request.endopoint = std::make_shared<Socket::Endpoint>(*cached);
			request.endopoint->port(request.url.port());
		}
	}
	fetch->fetch();
}

std::shared_ptr<Fetch> Fetcher::fetch(Request&& request) {
	auto r = Fetch::create(this, std::move(request));
	if (!stopped_) {
		io().post([r, this] {
			fetchImpl(r);
		});
	}
	return r;
}

void Fetcher::cacheEndpoint(const Request& x) {
	if (x.url.host().empty()) {
		return;
	}
	endpointCache_->set(x.url.host(), x.endopoint);
}

void Fetcher::reuseSocket(const Request& x) {
	if (x.url.id().empty()) {
		return;
	}
	socketCache_->add(x.url.id(), std::move(x.socket));
}

} // namespace NovHttp
