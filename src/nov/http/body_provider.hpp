#pragma once

#include <deque>
#include <functional>
#include <memory>
#include <string>

#include <boost/asio/io_service.hpp>

#include <nov/http/buffer.hpp>

namespace NovHttp {

class Fetch;
class Fetcher;
class RequestWriter;

class BodyWriter {
public:
	explicit BodyWriter(RequestWriter* writer): writer_(writer) {
	}

	void writeChunkHeader(std::size_t size);
	void writeChunk(std::shared_ptr<Buffer> buffer);
	void writeChunkEnd();
	void writeZeroChunkHeader();

private:
	RequestWriter* writer_ = nullptr;
};

class BodyProvider {
public:
	virtual ~BodyProvider() {}

	// Return 0 if chunked
	virtual std::size_t size() = 0;
	virtual bool more(BodyWriter& writer) = 0;
	virtual void start(Fetch*) {}
};

class StringBodyProvider: public BodyProvider {
public:
	explicit StringBodyProvider(std::string data);
	static std::shared_ptr<StringBodyProvider> create(std::string data);

	std::size_t size() override;
	bool more(BodyWriter& writer) override;

private:
	std::size_t size_ = 0;
	std::shared_ptr<Buffer> buffer_;
};

class UnknownBodyProvider: public BodyProvider {
public:
	UnknownBodyProvider(boost::asio::io_service* io, std::size_t size);
	static std::shared_ptr<UnknownBodyProvider> create(boost::asio::io_service* io, std::size_t size);

	std::size_t size() override;
	bool more(BodyWriter& writer) override;
	void start(Fetch* fetch) override;

	void add(std::shared_ptr<Buffer> buffer);
	void add(std::string data);

	void done();

private:
	void addImpl(std::shared_ptr<Buffer> buffer);
	void resume();

	std::deque<std::shared_ptr<Buffer> > buffers_;
	bool done_ = false;
	bool doneReturned_ = false;
	std::size_t size_ = 0;
	std::weak_ptr<Fetch> fetch_;
	boost::asio::io_service* io_ = nullptr;
	std::size_t partSize_ = 0;
};

} // namespace NovHttp
