#include <sstream>

#include <nov/http/buffer_helper.hpp>

#include "request_writer.hpp"

namespace NovHttp {

void RequestWriter::writeBody(const CStringView& chunk) {
	chain_.write(chunk);
}

void RequestWriter::writeBody(std::shared_ptr<Buffer> buffer) {
	chain_.write(std::move(buffer));
}

void RequestWriter::writeBodyString(std::string chunk) {
	auto str = std::make_shared<std::string>(std::move(chunk));
	StringView view(const_cast<char*>(str->data()), str->size());
	std::shared_ptr<Buffer> buffer(new Buffer(view), [str] (Buffer* b) {
		delete b;
	});
	writeBody(std::move(buffer));
}

void RequestWriter::writeChunkSize(std::size_t size) {
	std::stringstream oss;
	oss << std::hex << size << "\r\n";
	chain_.write(BufferHelper::create(oss.str()));
}

void RequestWriter::writeChunkEnd() {
	chain_.write("\r\n");
}

void RequestWriter::writeBodyEnd() {
	chain_.write("0\r\n\r\n");
}

} // namespace NovHttp
