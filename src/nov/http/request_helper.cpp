#include "request_helper.hpp"

#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zlib.hpp>

#include <nov/http/filter_response_decorator.hpp>
#include <nov/http/headers.hpp>

namespace NovHttp {

namespace {

class FilterSelector: public ResponseDecorator {
public:

	explicit FilterSelector(std::unique_ptr<ResponseHandler> original):
		ResponseDecorator(original.get()),
		original_(std::move(original)) {
	}

	void header(const CStringView& key, const CStringView& value) override {
		if(key.iEquals(Headers::ContentEncoding)) {
			if (value.iEquals(HeaderValues::ContentEncodingDeflate)) {
				holder_.reset(new FilterResponseDecorator<boost::iostreams::zlib_decompressor>(original_.get()));
				handler_ = holder_.get();
			} else if (value.iEquals(HeaderValues::ContentEncodingGzip)) {
				holder_.reset(new FilterResponseDecorator<boost::iostreams::gzip_decompressor>(original_.get()));
				handler_ = holder_.get();
			}
		}
		return original_->header(key, value);
	}

private:
	std::unique_ptr<ResponseHandler> original_;
	std::unique_ptr<ResponseHandler> holder_;
};

} // namespace

std::unique_ptr<ResponseHandler> RequestHelper::makeDecoder(std::unique_ptr<ResponseHandler> handler) {
	return std::unique_ptr<ResponseHandler>(new FilterSelector(std::move(handler)));
}

void RequestHelper::addDecoder(Request& request) {
	request.headers[Headers::AcceptEncoding] = {HeaderValues::ContentEncodingDeflate, HeaderValues::ContentEncodingGzip};
	request.handler = makeDecoder(std::move(request.handler));
}

void RequestHelper::addClose(Request& request) {
	request.headers[Headers::Connection] = {HeaderValues::ConnectionClose};
	request.connectionClose = true;
}

Request RequestHelper::createFromUrl(const std::string& url) {
	Request req;
	if (!req.url.parse(url)) {
		return req;
	}
	req.method = "GET";

	if (!req.url.isIp()) {
		req.headers["Host"] = {req.url.hostHeader()};
	}
	return req;
}

}  // namespace NovHttp
