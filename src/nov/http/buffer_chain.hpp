#pragma once

#include <deque>

#include <nov/http/buffer.hpp>
#include <nov/http/buffer_allocator.hpp>

namespace NovHttp {

class BufferChain {
public:
	typedef std::deque<std::shared_ptr<Buffer> > Chain;

	explicit BufferChain(BufferAllocator allocator): allocator_(allocator) {
	}

	void write(CStringView chunk) {
		std::size_t size = 0;
		while (chain_.empty() || (size = chain_.back()->writeSome(chunk)) != chunk.size) {
			chunk.size -= size;
			chunk.pointer += size;
			chain_.emplace_back(allocator_.allocate(chunk.size));
		}
	}

	void write(std::shared_ptr<Buffer> b) {
		chain_.emplace_back(std::move(b));
	}

	bool readCompleted(std::size_t size) {
		while (size > 0) {
			auto& first = firstBuffer();
			auto view = first.read(size);
			size -= view.size;
			if (first.content().size < 1) {
				chain_.pop_front();
			}
		}
		return !chain_.empty();
	}

	Buffer& firstBuffer() {
		return *chain_.front();
	}

	const Chain& chain() const {
		return chain_;
	}

	std::string toString() const {
		std::string result;
		for (auto& x: chain_) {
			result += x->content().toString();
		}
		return result;
	}

private:
	Chain chain_;
	BufferAllocator allocator_;
};

} // namespace NovHttp
