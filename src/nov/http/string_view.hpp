#pragma once

#include <algorithm>
#include <ostream>
#include <string>

namespace NovHttp {

template<typename T>
struct StringViewBase {
	T pointer = nullptr;
	std::size_t size = 0;

	template<std::size_t Size>
	inline StringViewBase(const char(&x)[Size]): pointer(x), size(Size-1) {}
	inline StringViewBase() {}
	inline StringViewBase(T p, std::size_t s): pointer(p), size(s) {}

	template<typename Y>
	inline StringViewBase(const StringViewBase<Y>& other): pointer(other.pointer), size(other.size) {}

	template<std::size_t Size>
	inline bool equals(const char(&x)[Size]) const {
		if (Size-1 != size) {
			return false;
		}
		return std::equal(pointer, pointer+size, x);
	}

	template<std::size_t Size>
	inline bool iEquals(const char(&x)[Size]) const {
		if (Size-1 != size) {
			return false;
		}
		return std::equal(pointer, pointer+size, x, [] (char lhs, char rhs) {
			return std::tolower(lhs) == std::tolower(rhs);
		});
	}

	inline std::string toString() const {
		return std::string(pointer, size);
	}

	template<typename Y>
	StringViewBase<Y> cast() const {
		return StringViewBase<Y>{pointer, size};
	}

	inline bool isValid() const {
		return pointer != nullptr;
	}
};

using StringView = StringViewBase<char *>;
using CStringView = StringViewBase<const char *>;

} // namespace NovHttp

namespace std {

template<typename T>
ostream& operator<<(ostream& out, const NovHttp::StringViewBase<T>& x) {
	out.write(x.pointer, x.size);
	return out;
}

} // namespace std
