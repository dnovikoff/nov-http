#pragma once

#include <cstddef>

namespace NovHttp {

struct BodyInfo {
	bool expectBody = false;
	bool chunked = false;
	// This is Content-Length (actually transfered with http)
	std::size_t length = 0;
};

} // namespace NovHttp
