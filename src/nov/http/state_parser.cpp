#include "state_parser.hpp"

#include <nov/http/response_handler.hpp>
#include <nov/http/headers.hpp>

namespace NovHttp {

Parser::State StateParser::parse(Buffer& buffer) {
	if (state_ == Parser::Done || state_ == Parser::Failed) {
		return state_;
	}
	parser_.setBuffer(buffer);
	auto state = state_;
	while (state != Parser::Pause) {
		state = parseStep();
		if (state == Parser::Done) {
			if (!handler_->done()) {
				state = Parser::Failed;
			} else {
				break;
			}
		}
		if (state == Parser::Failed) {
			handler_->fail(FailReason::ParseError, "Error parsing response");
			break;
		}
	}
	return state;
}

Parser::State StateParser::parseStep() {
	switch (state_) {
	case Parser::Body:
		return onBody();
	case Parser::ChunkStart:
		return onChunkStart();
	case Parser::ChunkMiddle:
		return onChunkMiddle();
	case Parser::Header:
		return onHeader();
	case Parser::StatusLine:
		return onStatusLine();
	case Parser::Done:
	case Parser::Failed:
		return state_;
	case Parser::Pause:
	case Parser::HeaderMore:
		// Not possible
		return state_ = Parser::Failed;
	}
	return state_ = Parser::Failed;
}

Parser::State StateParser::onBody() {
	CStringView chunk;
	auto next = parser_.parseBody(bodyInfo_.length, chunk);
	if (chunk.size > 0) {
		if (!handler_->body(chunk)) {
			return state_ = Parser::Failed;
		}
		bodyInfo_.length -= chunk.size;
		if (bodyInfo_.length == 0) {
			return state_ = Parser::Done;
		}
	}
	if (next == Parser::Pause) {
		return next;
	}
	return state_ = next;
}

Parser::State StateParser::onChunkMiddle() {
	CStringView chunk;
	auto next = parser_.parseChunkMiddle(chunkLen_, chunk);
	if (chunk.size > 0) {
		handler_->body(chunk);
		chunkLen_ -= chunk.size;
	}
	if (next == Parser::Pause) {
		return next;
	} else if (next == Parser::ChunkStart && lastChunk_) {
		return state_ = Parser::Done;
	}
	return state_ = next;
}

Parser::State StateParser::onChunkStart() {
	auto next = parser_.parseChunkHeader(chunkLen_);
	if (next == Parser::Pause) {
		return next;
	}
	if (chunkLen_ == 0) {
		lastChunk_ = true;
	}
	return state_ = next;
}

Parser::State StateParser::onHeader() {
	CStringView key;
	CStringView value;
	auto next = parser_.parseHeader(key, value);
	if (next == Parser::Header) {
		headersStarted_ = true;
		// TODO: wrong condition. Correct !=identity
		if (key.iEquals(Headers::TransferEncoding) && value.equals(HeaderValues::TransferEncodingChunked)) {
			if (chunkLen_ > 0) {
				return state_ = Parser::Failed;
			}
			bodyInfo_.chunked = true;
		} else if (key.iEquals(Headers::ContentLength)) {
			if (!parser_.parseLength(value, bodyInfo_.length)) {
				return state_ = Parser::Failed;
			}
			haveContentLength_ = true;
		}
		handler_->header(key, value);
	} else if (next == Parser::Pause) {
		return next;
	} else if (next == Parser::Body) {
		if (!bodyInfo_.expectBody) {
			state_ = Parser::Done;
		} else if (bodyInfo_.chunked) {
			state_ = Parser::ChunkStart;
		} else if (haveContentLength_) {
			state_ = Parser::Body;
		} else {
			return state_ = Parser::Failed;
		}
		handler_->headersDone(bodyInfo_);
		return state_;
	} else if (next == Parser::HeaderMore) {
		if (!headersStarted_) {
			return state_ = Parser::Failed;
		}
		handler_->headerValueNextLine(value);
		return state_;
	}
	return state_ = next;
}

Parser::State StateParser::onStatusLine() {
	CStringView version;
	CStringView reason;
	int code = 0;
	Parser::State next = parser_.parseStatus(version, code, reason);
	if (next == Parser::Header) {
		handler_->status(version, code, reason);
		if (code == 204 || code == 304 || (code >= 100 && code <= 199)) {
			bodyInfo_.expectBody = false;
		}
	} else if (next == Parser::Pause) {
		return next;
	}
	return state_ = next;
}

} // namespace NovHttp
