#include "url.hpp"

#include <boost/regex.hpp>

namespace NovHttp {

void Url::setPath(const std::string& path) {
	path_ = path;
	if (path_.empty()) {
		path_ = "/";
	}
}

bool Url::parse(const std::string& url) {
	boost::smatch match;
	boost::regex regexp(R"=(^(http|https)://([^/:]+)(:(\d+))?(/.*)?$)=");
	isValid_ = boost::regex_match(url, match, regexp);
	if (!isValid_) {
		return false;
	}

	isSsl_ = (match[1] != "http");
	portStr_ = match[4];
	host_ = match[2];
	hostHeader_ = host_;
	if (portStr_.empty()) {
		portStr_ = isSsl_?"443":"80";
	} else {
		hostHeader_ += ":" + portStr_;
	}
	path_ = match[5];
	if (path_.empty()) {
		path_ = "/";
	}
	port_ = std::atoi(portStr_.c_str());
	id_ = (isSsl_?"s:":"") + host_ + ":" + portStr_;
	{
		boost::regex ipRegex(R"=(^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$)=");
		isIp_ = boost::regex_match(host_, match, ipRegex);
	}
	return true;
}

Url Url::create(const std::string& url) {
	Url u;
	u.parse(url);
	return u;
}

} // namespace NovHttp
