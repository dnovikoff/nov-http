#include "body_provider.hpp"

#include <nov/http/buffer_helper.hpp>
#include <nov/http/fetch.hpp>
#include <nov/http/fetcher.hpp>
#include <nov/http/make_unique.hpp>
#include <nov/http/request_writer.hpp>

namespace NovHttp {

void BodyWriter::writeChunkHeader(std::size_t size) {
	writer_->writeChunkSize(size);
}

void BodyWriter::writeChunk(std::shared_ptr<Buffer> buffer) {
	writer_->writeBody(std::move(buffer));
}

void BodyWriter::writeChunkEnd() {
	writer_->writeChunkEnd();
}

void BodyWriter::writeZeroChunkHeader() {
	writer_->writeBodyEnd();
}

StringBodyProvider::StringBodyProvider(std::string data): size_(data.size()), buffer_(BufferHelper::create(std::move(data))) {
}

std::size_t StringBodyProvider::size() {
	return size_;
}

bool StringBodyProvider::more(BodyWriter& writer) {
	if (buffer_) {
		writer.writeChunk(std::move(buffer_));
	}
	return false;
}

UnknownBodyProvider::UnknownBodyProvider(boost::asio::io_service* io, std::size_t size): size_(size), io_(io) {
}

std::size_t UnknownBodyProvider::size() {
	// Optimization
	if (done_ && !size_) {
		size_ = partSize_;
	}
	return size_;
}

bool UnknownBodyProvider::more(BodyWriter& writer) {
	if (!size_ && partSize_) {
		writer.writeChunkHeader(partSize_);
	}
	for (auto& x: buffers_) {
		writer.writeChunk(std::move(x));
	}
	if (!size_ && partSize_) {
		writer.writeChunkEnd();
	}
	partSize_ = 0;
	buffers_.clear();
	if (!doneReturned_ && done_ && !size_) {
		writer.writeZeroChunkHeader();
		doneReturned_ = true;
	}
	return !done_;
}

void UnknownBodyProvider::start(Fetch* fetch) {
	fetch_ = fetch->shared_from_this();
}

void UnknownBodyProvider::add(std::shared_ptr<Buffer> buffer) {
	io_->post([this, buffer] {
		addImpl(buffer);
	});
}

void UnknownBodyProvider::add(std::string data) {
	add(BufferHelper::create(std::move(data)));
}

void UnknownBodyProvider::done() {
	io_->post([this] {
		done_ = true;
		resume();
	});
}

void UnknownBodyProvider::resume() {
	auto fetch = fetch_.lock();
	if (fetch) {
		fetch->resumeBodySend();
	}
}

void UnknownBodyProvider::addImpl(std::shared_ptr<Buffer> buffer) {
	partSize_ += buffer->length();
	buffers_.emplace_back(std::move(buffer));
	resume();
}

std::shared_ptr<StringBodyProvider> StringBodyProvider::create(std::string data) {
	return std::make_shared<StringBodyProvider>(std::move(data));
}

std::shared_ptr<UnknownBodyProvider> UnknownBodyProvider::create(boost::asio::io_service* io, std::size_t size) {
	return std::make_shared<UnknownBodyProvider>(io, size);
}

} // namespace NovHttp
