#include "buffer_helper.hpp"

namespace NovHttp {

std::shared_ptr<Buffer> BufferHelper::create(std::string data) {
	auto str = std::make_shared<std::string>(std::move(data));
	StringView view(const_cast<char*>(str->c_str()), str->size());
	std::shared_ptr<Buffer> ptr(new Buffer(view), [str] (Buffer* b) {
		delete b;
	});
	ptr->add(str->size());
	return ptr;
}

} // namespace NovHttp
