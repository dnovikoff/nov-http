#pragma once

#include <memory>

#include <boost/asio.hpp>

#include <nov/http/request.hpp>
#include <nov/http/state_parser.hpp>
#include <nov/http/request_writer.hpp>
#include <nov/http/timer.hpp>
#include <nov/http/fetch_response_decorator.hpp>
#include <nov/http/socket.hpp>

namespace NovHttp {

class Fetcher;

class Fetch: public std::enable_shared_from_this<Fetch> {
public:
	Fetch(Fetcher*, Request&& r);

	// Safe to call from other thread
	void cancel();

	Request& request() {
		return request_;
	}

	// Call this if fetch will have body
	void resumeBodySend();

private:
	enum State {
		NotStarted,
		Started,
		Paused,
		// Finishied states are >= Done
		Done,
		Failed,
		Canceled,
	};
	using Resolver = Socket::Resolver;

	friend class Fetcher;
	static std::shared_ptr<Fetch> create(Fetcher*, Request&&);

	void cancelImpl();
	void resumeBodySendImpl();

	void fetch();

	void start();
	void handshake();
	void beforeConnect();
	void onConnected();
	void connect(Socket::Endpoint endpoint);
	void connect(Resolver::iterator iterator);
	void init();
	void resolve();
	void close();
	void fail(FailReason reason, const std::string& message);
	CStringView viewFromString(const std::string& x);
	void writeRequest();
	void afterWrite();
	void afterParsed();
	void onRead();

	void startTimer(std::weak_ptr<Timer>& timer, Timer::Duration dur, FailReason reason);
	void cancelTimer(std::weak_ptr<Timer>& timer);

	void done();
	void cancelTimers();

	// Returns true if more data
	bool readBodyData();

	inline Socket& socket() {
		return *request_.socket;
	}

	inline ResponseHandler& handler() {
		return decorator_;
	}

	bool isFinished() const;

	Fetcher* fetcher_ = nullptr;

	Request request_;
	FetchResponseDecorator decorator_;
	StateParser parser_;
	RequestWriter writer_;
	BodyWriter body_;
	bool moreBodyExpected_ = false;
	std::shared_ptr<Buffer> buffer_;
	State state_ = NotStarted;
	bool isSsl = false;
	std::weak_ptr<Timer> phaseTimer_;
	std::weak_ptr<Timer> totalTimer_;
	std::shared_ptr<Resolver> resolver_;
	bool writeDone_ = false;
	bool connectionReused_ = false;
};


} // namespace NovHttp
