#include "buffer_allocator.hpp"

namespace NovHttp {

std::shared_ptr<Buffer> BufferAllocator::allocate(std::size_t size) {
	size = std::max(size, minSize_);
	auto* mem = new char[size];
	auto view = StringView(mem, size);
	Buffer* buf = new Buffer(view);
	return std::shared_ptr<Buffer>(buf, [mem] (Buffer* ptr) {
		delete ptr;
		delete[] mem;
	});
}

} // namespace NovHttp
