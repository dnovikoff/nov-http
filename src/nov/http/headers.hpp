#pragma once

namespace NovHttp {

namespace Headers {
const char TransferEncoding[] = "Transfer-Encoding";
const char ContentLength[] = "Content-Length";
const char Connection[] = "Connection";
const char ContentEncoding[] = "Content-Encoding";
const char AcceptEncoding[] = "Accept-Encoding";
const char Location[] = "Location";
} // namespace Headers

namespace HeaderValues {
const char TransferEncodingChunked[] = "chunked";
const char ConnectionClose[] = "close";
const char ContentEncodingDeflate[] = "deflate";
const char ContentEncodingGzip[] = "gzip";
} // namespace HeaderValues

} // namespace NovHttp
