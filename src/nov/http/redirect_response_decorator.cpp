#include "redirect_response_decorator.hpp"

#include <nov/http/headers.hpp>
#include <nov/http/fetcher.hpp>

namespace NovHttp {

RedirectResponseDecorator::RedirectResponseDecorator(std::unique_ptr<Data> data): data_(std::move(data)) {
	data_->prototype.socket.reset();
	data_->prototype.endopoint.reset();
	redirect_ = (data_->prototype.method == "GET" || data_->prototype.method == "HEAD");
}

void RedirectResponseDecorator::resolved() {
	return data_->original->resolved();
}

void RedirectResponseDecorator::connected() {
	return data_->original->connected();
}

void RedirectResponseDecorator::handshake() {
	return data_->original->handshake();
}

void RedirectResponseDecorator::started() {
	return data_->original->started();
}

void RedirectResponseDecorator::status(const CStringView& c, int code, const CStringView& r) {
	if (code == 301 || code == 302) {
		return;
	}
	redirect_ = false;
	return data_->original->status(c, code, r);
}

void RedirectResponseDecorator::header(const CStringView& key, const CStringView& value) {
	if (!redirect_) {
		return data_->original->header(key, value);
	}
	if (key.iEquals(Headers::Location)) {
		location_ = value.toString();
	}
	return;
}

void RedirectResponseDecorator::headerValueNextLine(const CStringView& valueChunk) {
	if (!redirect_) {
		return data_->original->headerValueNextLine(valueChunk);
	}
}

void RedirectResponseDecorator::headersDone(const BodyInfo& info) {
	if (!redirect_) {
		return data_->original->headersDone(info);
	}
}

bool RedirectResponseDecorator::body(const CStringView& chunk) {
	if (!redirect_) {
		return data_->original->body(chunk);
	}
	return true;
}

bool RedirectResponseDecorator::done() {
	if (!redirect_) {
		return data_->original->done();
	}
	return true;
}

void RedirectResponseDecorator::fail(FailReason reason, const std::string& message) {
	redirect_ = false;
	return data_->original->fail(reason, message);
}

void RedirectResponseDecorator::finalize() {
	if (!redirect_) {
		return;
	}
	if (data_->attempts-- == 0) {
		data_->original->fail(FailReason::TooMuchRedirect, "Too much redirects. Last is: " + location_);
		return;
	}
	auto request = data_->prototype.copy();
	if (!request.url.parse(location_)) {
		request.url.setPath(location_);
	}
	auto *fetcher = data_->fetcher;
	request.handler.reset(new RedirectResponseDecorator(std::move(data_)));
	fetcher->fetch(std::move(request));
}

} // namespace NovHttp
