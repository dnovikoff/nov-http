#pragma once

#include <list>
#include <map>
#include <memory>
#include <string>

#include <chrono>

namespace NovHttp {

template<typename KeyType, typename Endpoint>
class EndpointCache {
private:
	using ValuePtr = std::shared_ptr<Endpoint>;
	using Time = std::chrono::steady_clock::time_point;

	class Data {
	public:
		KeyType key;
		Time startTime;
		ValuePtr ptr;
	};

	using ListType = typename std::list<Data>;
	using MapType = std::map<KeyType, typename ListType::iterator>;

public:
	using Duration = std::chrono::milliseconds;

	EndpointCache(std::size_t max, Duration time): maxSize_(max), maxDuration_(time) {}

	ValuePtr get(const KeyType& key) {
		auto it = map_.find(key);
		if (it == map_.end()) {
			return ValuePtr();
		}
		auto listIt = it->second;
		auto data = std::move(*listIt);
		list_.erase(listIt);
		if (now() > (data.startTime + maxDuration_)) {
			map_.erase(it);
			return ValuePtr();
		}
		list_.push_front(std::move(data));
		it->second = list_.begin();
		return list_.begin()->ptr;
	}

	void set(const KeyType& key, ValuePtr value) {
		auto it = map_.find(key);
		if (it != map_.end()) {
			auto listIt = it->second;
			if (listIt->ptr == value) {
				return;
			}
			list_.erase(listIt);
			map_.erase(key);
		}
		Data data;
		data.ptr = std::move(value);
		data.key = key;
		data.startTime = now();
		list_.push_front(std::move(data));
		map_[key] = list_.begin();

		while (size() > maxSize_) {
			auto it = list_.end();
			--it;
			map_.erase(it->key);
			list_.erase(it);
		}
	}

	std::size_t size() const {
		return list_.size();
	}

private:
	Time now() const {
		return std::chrono::steady_clock::now();
	}

	MapType map_;
	ListType list_;
	std::size_t maxSize_ = 1024;
	Duration maxDuration_;
};

} // namespace NovHttp
