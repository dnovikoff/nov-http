#pragma once

#include <queue>

#include <nov/http/buffer_allocator.hpp>
#include <nov/http/buffer_chain.hpp>

namespace NovHttp {

class RequestWriter {
public:
	explicit RequestWriter(BufferAllocator allocator): chain_(allocator) {
	}

	void writeRequest(const CStringView& method, const CStringView& url) {
		return writeAll(method, " ", url, " HTTP/1.1\r\n");
	}

	void writeHeader(const CStringView& key, const CStringView& value) {
		return writeAll(key, ": ", value, "\r\n");
	}

	void writeHeaderValueNextLine(const CStringView& chunk) {
		return writeAll(" ", chunk, "\r\n");
	}

	void writeHeaderEnd() {
		return writeAll("\r\n");
	}

	const BufferChain& chain() const {
		return chain_;
	}

	bool empty() const {
		return chain().chain().empty();
	}

	bool readCompleted(std::size_t size) {
		return chain_.readCompleted(size);
	}

	void writeChunkSize(std::size_t size);
	void writeChunkEnd();

	void writeBody(const CStringView& chunk);
	void writeBody(std::shared_ptr<Buffer> buffer);
	void writeBodyString(std::string chunk);
	void writeBodyEnd();

private:
	void writeAll() {
	}

	template<typename ...Args>
	void writeAll(const CStringView& first, Args&&... other) {
		chain_.write(first);
		return writeAll(std::forward<Args>(other)...);
	}

	BufferChain chain_;
};

} // namespace NovHttp
