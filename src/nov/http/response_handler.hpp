#pragma once

#include <string>
#include <nov/http/string_view.hpp>
#include <nov/http/body_info.hpp>

namespace NovHttp {

enum class FailReason {
	ResolveError,
	ConnectError,
	ParseError,
	SendError,
	ReadError,
	Canceled,
	SslHandshakeError,
	ResolveTimeout,
	ConnectTimeout,
	HandshakeTimeout,
	ReadTimeout,
	WriteTimeout,
	Timeout,
	DecodeError,
	TooMuchRedirect,
};

const char* enumValue(FailReason reason);

class ResponseHandler {
public:
	virtual ~ResponseHandler() = 0;

	// resolved, connected, handshake, started could be called several times
	// Ex. in case of using redirects, or reconnect in case of reusing connection

	// This handler is called, when hostname esolved
	// It could be skipped if the IP was provided (or host ip cached)
	virtual void resolved() {}

	// Called when connected
	// Could be skipped if connection keep-alived
	virtual void connected() {}

	// Called only for SSL connections
	// Could be skipped if connection keep-alived
	virtual void handshake() {}

	// Called when request starts
	virtual void started() {}

	virtual void status(const CStringView& version, int code, const CStringView& reason) = 0;
	virtual void header(const CStringView& key, const CStringView& value) = 0;
	// Special case for multiple-line headers
	virtual void headerValueNextLine(const CStringView& valueChunk) = 0;

	// Called once, when headers done
	virtual void headersDone(const BodyInfo&) = 0;

	// Called when new data comes from
	virtual bool body(const CStringView& chunk) = 0;

	// Called only once if no errors occured.
	// Will be the last call (if called)
	// Should return false if error occured (connection will be closed)
	// Note: if you return false, no "fail" will be called.
	virtual bool done() = 0;

	// Called only once if any error occured
	// Will be the last call (if called)
	// Can be called at any stage
	virtual void fail(FailReason reason, const std::string& message) = 0;

	// Called when connection released
	virtual void finalize() {}
};

} // namespace NovHttp
