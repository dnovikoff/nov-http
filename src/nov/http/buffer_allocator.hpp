#pragma once

#include <memory>

#include <nov/http/buffer.hpp>

namespace NovHttp {

class BufferAllocator {
public:
	explicit BufferAllocator(std::size_t minSize): minSize_(minSize) {
	}

	std::shared_ptr<Buffer> allocate(std::size_t atLeast);

private:
	std::size_t minSize_ = 0;
};

} // namespace NovHttp
