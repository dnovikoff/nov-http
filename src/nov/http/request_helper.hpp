#pragma once

#include <nov/http/request.hpp>

namespace NovHttp {

class RequestHelper {
public:
	static std::unique_ptr<ResponseHandler> makeDecoder(std::unique_ptr<ResponseHandler> handler);
	static void addDecoder(Request& request);
	static void addClose(Request& request);
	static Request createFromUrl(const std::string& url);
};

}  // namespace NovHttp
