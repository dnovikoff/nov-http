file(GLOB headers ${CMAKE_CURRENT_SOURCE_DIR}/*.hpp)
file(GLOB sources ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp ${CMAKE_CURRENT_SOURCE_DIR}/*.hpp)

add_library(nov_http STATIC ${sources})
target_link_libraries(nov_http ${Boost_SYSTEM_LIBRARY} ${Boost_IOSTREAMS_LIBRARY} ${Boost_REGEX_LIBRARY} crypto ssl)

install(TARGETS nov_http ARCHIVE DESTINATION lib)
install(FILES ${headers} DESTINATION include/nov/http COMPONENT dev)