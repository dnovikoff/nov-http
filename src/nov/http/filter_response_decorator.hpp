#pragma once

#include <memory>
#include <sstream>

#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/filtering_stream.hpp>

#include <nov/http/response_decorator.hpp>

namespace NovHttp {

class FilterWriteSink: public boost::iostreams::sink {
public:
	explicit FilterWriteSink(ResponseHandler* handler): handler_(handler) {
	}

	std::streamsize write(const char* s, std::streamsize n) {
		handler_->body(CStringView{s, std::size_t(n)});
		return n;
	}

private:
	ResponseHandler* handler_ = nullptr;
};

template<typename Filter>
class FilterResponseDecorator: public ResponseDecorator {
public:
	explicit FilterResponseDecorator(ResponseHandler* handler): ResponseDecorator(handler) {
	}

	bool body(const CStringView& chunk) override {
		stream_ << chunk;
		return true;
	}

	bool done() override {
		Filter filter;
		boost::iostreams::filtering_istream in;
		in.push(filter);
		in.push(stream_);

		FilterWriteSink sink(handler_);
		try {
			boost::iostreams::copy(in, sink);
		} catch (const std::ios::failure& error) {
			handler_->fail(FailReason::DecodeError, error.what());
			return false;
		}
		return handler_->done();
	}

private:
	std::stringstream stream_;
};

} // namespace NovHttp
