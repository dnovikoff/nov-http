#pragma once

#include <list>
#include <map>
#include <memory>
#include <string>

#include <chrono>

namespace NovHttp {


template<typename Socket>
class SocketCache {
private:
	using ValuePtr = std::shared_ptr<Socket>;
	using Time = std::chrono::system_clock::time_point;

	class Data;
	using ListType = std::list<Data>;
	using MapType = std::multimap<std::string, typename ListType::iterator>;

	class Data {
	public:
		std::string key;
		Time startTime;
		ValuePtr ptr;
		typename MapType::iterator iterator;
	};

public:
	using Duration = std::chrono::system_clock::duration;

	SocketCache(std::size_t max, Duration time): maxSize_(max), maxDuration_(time) {}

	ValuePtr get(const std::string& key) {
		while (true) {
			auto it = map_.find(key);
			if (it == map_.end()) {
				break;
			}
			Data data = std::move(*it->second);
			list_.erase(it->second);
			map_.erase(it);
			if (now() < (data.startTime + maxDuration_)) {
				return data.ptr;
			}
		}
		return ValuePtr();
	}

	void add(const std::string& key, ValuePtr value) {
		Data data;
		data.ptr = std::move(value);
		data.key = key;
		data.startTime = now();
		list_.push_front(std::move(data));
		auto listIt = list_.begin();
		list_.front().iterator = map_.insert(typename MapType::value_type(key, listIt));
		while (size() > maxSize_) {
			auto it = list_.end();
			--it;
			map_.erase(it->iterator);
			list_.erase(it);
		}
	}

	std::size_t size() const {
		return list_.size();
	}

private:
	Time now() const {
		return std::chrono::system_clock::now();
	}

	MapType map_;
	ListType list_;
	std::size_t maxSize_ = 1024;
	Duration maxDuration_;
};

} // namespace NovHttp
