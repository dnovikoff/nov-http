#pragma once

#include <cstring>

#include <nov/http/string_view.hpp>

namespace NovHttp {

class Buffer {
public:
	template<std::size_t Size>
	inline explicit Buffer(char(&x)[Size]): data_(StringView(x, Size)) {
	}

	inline explicit Buffer(StringView data): data_(data) {
	}

	inline void add(std::size_t size) {
		size_ += size;
	}

	void unsafeWrite(const CStringView& view) {
		memcpy(data_.pointer+size_, view.pointer, view.size);
		add(view.size);
	}

	bool tryWrite(const CStringView& view) {
		if (spaceLeft() < view.size) {
			return false;
		}
		unsafeWrite(view);
		return true;
	}

	std::size_t writeSome(const CStringView& view) {
		auto size = std::min(spaceLeft(), view.size);
		if (size > 0) {
			unsafeWrite(CStringView(view.pointer, size));
		}
		return size;
	}

	inline std::size_t spaceLeft() const {
		return data_.size - size_;
	}

	inline StringView buffer() const {
		return StringView{data_.pointer + size_, spaceLeft()};
	}


	inline CStringView content() const {
		return CStringView{data_.pointer + position_, size_-position_};
	}

	StringView buffer(std::size_t atLeast) {
		if (spaceLeft() < atLeast) {
			cleanup();
			if (spaceLeft() < atLeast) {
				return StringView{};
			}
		}
		return buffer();
	}

	bool cleanup() {
		const auto bytesLeft = size_ - position_;
		memmove(data_.pointer, data_.pointer + position_, bytesLeft);
		size_ = bytesLeft;
		position_ = 0;
		if (size_ == data_.size) {
			broken_ = true;
			return false;
		}
		return true;
	}

	void skip(std::size_t size) {
		position_ += size;
	}

	template<typename Result=CStringView>
	inline Result untilOneOf(std::initializer_list<char> input) {
		if (size_ < 1) {
			return Result{};
		}
		for (auto i = position_; i < size_; ++i) {
			for (auto x: input) {
				if (data_.pointer[i] == x) {
					auto r = Result{data_.pointer+position_, i-position_};
					position_ = i+1;
					return r;
				}
			}
		}
		return Result{};
	}

	template<typename Result=CStringView>
	inline Result until(char input) {
		return untilOneOf<Result>({input});
	}

	inline std::size_t skipAnyOf(std::initializer_list<char> input) {
		if (input.size() < 1) {
			return 0;
		}
		auto i = position_;
		for (; i < size_; ++i) {
			bool shouldSkip = false;
			for (auto x: input) {
				if (data_.pointer[i] == x) {
					shouldSkip = true;
					break;
				}
			}

			if (!shouldSkip) {
				break;
			}
		}
		auto r = i-position_;
		position_ = i;
		return r;
	}

	inline CStringView tail() {
		auto r = CStringView{data_.pointer+position_, length()};
		position_ = 0;
		size_ = 0;
		return r;
	}

	inline bool isBroken() const {
		return broken_;
	}

	inline bool isEmpty() const {
		return size_ == position_;
	}

	template<typename Result=CStringView>
	inline Result tryRead(std::size_t size) {
		if (length() < size) {
			return  Result{};
		}
		auto r = Result{data_.pointer+position_, size};
		skip(size);
		return r;
	}

	template<typename Result=CStringView>
	inline Result read(std::size_t size) {
		size = std::min(size, length());
		auto r = Result{data_.pointer+position_, size};
		skip(size);
		return r;
	}

	inline std::size_t length() const {
		return size_-position_;
	}

private:
	StringView data_;
	std::size_t position_ = 0;
	std::size_t size_ = 0;
	bool broken_ = false;
};

} // namespace NovHttp
