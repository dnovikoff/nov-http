#pragma once

#include <chrono>
#include <functional>
#include <map>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include <nov/http/response_handler.hpp>
#include <nov/http/body_provider.hpp>
#include <nov/http/socket.hpp>
#include <nov/http/url.hpp>

namespace NovHttp {

struct HeaderComparator: std::binary_function<std::string, std::string, bool> {
	bool operator()(const std::string & s1, const std::string & s2) const {
		return boost::lexicographical_compare(s1, s2, boost::is_iless());
	}
};

struct Timeouts {
	using Duration = std::chrono::milliseconds;

	static Duration zero() {
		return std::chrono::milliseconds(0);
	}

	Duration resolve = zero();
	Duration connect = zero();
	Duration handshake = zero();
	Duration total = zero();
	Duration idle = zero();

	static Duration ms(std::size_t m) {
		return std::chrono::milliseconds(m);
	}

	void setAll(Duration dur) {
		resolve = dur;
		connect = dur;
		handshake = dur;
		total = dur;
		idle = dur;
	}

	void setAllMs(std::size_t m) {
		setAll(ms(m));
	}
};

struct Request {
	using Headers = std::map<std::string, std::vector<std::string>, HeaderComparator>;
	Url url;
	std::shared_ptr<Socket::Endpoint> endopoint;
	std::shared_ptr<Socket> socket;
	std::string method;
	bool useCommaForMultipleHeaders = false;
	Headers headers;
	std::shared_ptr<BodyProvider> body;
	std::size_t contentLength = 0;
	std::unique_ptr<ResponseHandler> handler;
	Timeouts timeouts;
	bool connectionClose = false;

	bool expectBody() const {
		return method != "HEAD";
	}

	Request copy() const {
		Request tmp;
		tmp.url = url;
		tmp.method = method;
		tmp.useCommaForMultipleHeaders = useCommaForMultipleHeaders;
		tmp.headers = headers;
		tmp.contentLength = contentLength;
		// Should support multiple start call?
		// tmp.body = body;
		tmp.timeouts = timeouts;
		tmp.connectionClose = connectionClose;
		return tmp;
	}
};

} // namespace NovHttp
