#pragma once

#include <nov/http/parser.hpp>
#include <nov/http/body_info.hpp>

namespace NovHttp {

class ResponseHandler;

class StateParser {
public:
	explicit StateParser(ResponseHandler* handler, bool withBody): handler_(handler) {
		bodyInfo_.expectBody = withBody;
	}

	Parser::State parse(Buffer& buffer);

private:
	Parser::State parseStep();
	Parser::State onBody();
	Parser::State onChunkMiddle();
	Parser::State onChunkStart();
	Parser::State onHeader();
	Parser::State onStatusLine();

	Parser parser_;
	Parser::State state_ = Parser::StatusLine;
	ResponseHandler* handler_ = nullptr;
	std::size_t chunkLen_ = 0;
	bool lastChunk_ = false;
	bool headersStarted_ = false;
	bool haveContentLength_ = false;
	BodyInfo bodyInfo_;
};

} // namespace NovHttp
