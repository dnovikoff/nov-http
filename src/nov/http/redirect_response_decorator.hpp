#pragma once

#include <memory>

#include <nov/http/request.hpp>
#include <nov/http/response_decorator.hpp>
#include <nov/http/string_view.hpp>

namespace NovHttp {

class RedirectResponseDecorator: public ResponseHandler {
public:
	struct Data {
		Request prototype;
		std::unique_ptr<ResponseHandler> original;
		std::size_t attempts = 0;
		Fetcher* fetcher = nullptr;
	};
	using OnHeadersDoneHandler = std::function<void(void)>;

	explicit RedirectResponseDecorator(std::unique_ptr<Data> data);

	void resolved() override;
	void connected() override;
	void handshake() override;
	void started() override;
	void status(const CStringView& c, int code, const CStringView& r) override;
	void header(const CStringView& key, const CStringView& value) override;
	void headerValueNextLine(const CStringView& valueChunk) override;
	void headersDone(const BodyInfo& info) override;
	bool body(const CStringView& chunk) override;
	bool done() override;
	void fail(FailReason reason, const std::string& message) override;
	void finalize() override;

private:
	std::unique_ptr<Data> data_;
	bool redirect_ = true;
	std::string location_;
};

} // namespace NovHttp
