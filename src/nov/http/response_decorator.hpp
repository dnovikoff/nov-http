#pragma once

#include <nov/http/response_handler.hpp>

namespace NovHttp {

class ResponseDecorator: public ResponseHandler {
public:
	void resolved() override {
		return handler_->resolved();
	}
	void connected() override {
		return handler_->connected();
	}
	void handshake() override {
		return handler_->handshake();
	}
	void started() override {
		return handler_->started();
	}
	void status(const CStringView& version, int code, const CStringView& reason) override {
		return handler_->status(version, code, reason);
	}
	void header(const CStringView& key, const CStringView& value) override {
		return handler_->header(key, value);
	}
	void headerValueNextLine(const CStringView& valueChunk) override {
		return handler_->headerValueNextLine(valueChunk);
	}
	void headersDone(const BodyInfo& info) override {
		return handler_->headersDone(info);
	}
	bool body(const CStringView& chunk) override {
		return handler_->body(chunk);
	}
	bool done() override {
		return handler_->done();
	}
	void fail(FailReason reason, const std::string& message) override {
		return handler_->fail(reason, message);
	}
	void finalize() override {
		return handler_->finalize();
	}

protected:
	explicit ResponseDecorator(ResponseHandler* handler): handler_(handler) {
	}

	ResponseHandler* handler_ = nullptr;
};

} // namespace NovHttp
