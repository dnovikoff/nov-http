#include "timer.hpp"

#include <boost/date_time/posix_time/ptime.hpp>

namespace NovHttp {

Timer::Timer(boost::asio::io_service& io, Duration dur, Handler h): timer_(io), duration_(dur), handler_(std::move(h)) {
}

void Timer::cancel() {
	timer_.cancel();
}

Timer::~Timer() {
}

std::weak_ptr<Timer> Timer::start(boost::asio::io_service& io, Duration dur, Handler h) {
	auto ptr = std::make_shared<Timer>(io, dur, h);
	ptr->start();
	return ptr;
}

void Timer::start() {
	timer_.expires_from_now(duration_);
	auto holder = shared_from_this();
	timer_.async_wait([this, holder] (const boost::system::error_code& e) {
		if (!handler_) {
			return;
		}
		handler_(e);
		handler_ = nullptr;
	});
}

} // namespace NovHttp
