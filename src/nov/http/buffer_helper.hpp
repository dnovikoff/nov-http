#pragma once

#include <memory>

#include <nov/http/buffer.hpp>

namespace NovHttp {

class BufferHelper {
public:
	// Should not be modified
	static std::shared_ptr<Buffer> create(std::string data);
};

} // namespace NovHttp
